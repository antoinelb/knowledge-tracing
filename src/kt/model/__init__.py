from .model import (  # noqa
    Config,
    ModelFct,
    add_comments,
    get_model,
)
