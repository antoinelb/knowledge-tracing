import jax
import jax.numpy as jnp
from ruamel.yaml import CommentedMap

from kt.utils.validators import set_field

from .base import Config as BaseConfig
from .base import InitFct, ModelFcts, PredictFct
from .layers import DenseParams, RnnParams, create_dense, create_rnn
from .losses import compute_binary_cross_entropy

#########
# types #
#########


class Config(BaseConfig):
    n_hid: list[int] = [256]

    _set_type = set_field("type", "dkt")


############
# external #
############


def add_comments(config: Config) -> CommentedMap:
    config_ = CommentedMap(config.dict())
    config_.yaml_add_eol_comment(
        "size of each hidden layer",
        "n_hid",
    )
    return config_


def create_model(config: Config) -> ModelFcts:
    init, predict = _create_model(config)
    return init, predict, compute_binary_cross_entropy  # type: ignore


############
# internal #
############


def _create_model(config: Config) -> tuple[InitFct, PredictFct]:
    rnn_layers = [create_rnn(act_fct=jax.nn.sigmoid) for _ in config.n_hid]
    dense_layer = create_dense()

    def init(
        key: jax.random.KeyArray, n_questions: int
    ) -> tuple[list[RnnParams], DenseParams]:
        keys = jax.random.split(key, len(config.n_hid) + 1)
        return (
            [
                init_(key_, n_in, n_out)
                for (init_, _), key_, n_in, n_out in zip(
                    rnn_layers,
                    keys[:-1],
                    [2 * n_questions, *config.n_hid[:-1]],
                    [*config.n_hid],
                )
            ],
            dense_layer[0](
                keys[-1],
                config.n_hid[-1] if config.n_hid else 2 * n_questions,
                n_questions,
            ),
        )

    def fct(
        params: tuple[list[RnnParams], DenseParams],
        key: jax.random.KeyArray,
        data: dict[str, jnp.ndarray],
        train: bool,
    ) -> dict[str, jnp.ndarray]:
        questions = data["questions"]
        prev_answers = data["prev_answers"]
        n_questions = params[1][1].shape[0]
        questions_ = jax.nn.one_hot(questions, n_questions)
        prev_answers = jnp.expand_dims(prev_answers, -1)
        inputs = jnp.concatenate(
            [questions_ * prev_answers, questions_ * (1 - prev_answers)], -1
        )

        rnn_keys = jax.random.split(key, len(params[0]))
        hidden = inputs
        for (_, fct_), params_, key_ in zip(rnn_layers, params[0], rnn_keys):
            hidden = fct_(params_, key_, hidden)

        outputs = jax.nn.sigmoid(dense_layer[1](params[1], hidden))

        # take the prediction corresponding to the asked question
        predictions = jnp.take_along_axis(
            outputs, jnp.expand_dims(questions - 1, -1), -1
        ).squeeze(-1)

        return {"predictions": predictions}

    return init, fct  # type: ignore
