import importlib
from pathlib import Path
from types import ModuleType

#########
# types #
#########


MODELS = [
    file.stem
    for file in Path(__file__).parent.glob("*.py")
    if file.stem not in ("__init__", "base", "layers", "losses", "utils")
]

default_model = "dkt"

############
# external #
############


def get_module(type_: str) -> ModuleType:
    try:
        return importlib.import_module(
            f"kt.model.models.{type_}",
        )
    except ModuleNotFoundError as exc:
        raise NotImplementedError(f"There is no model type {type_}.") from exc
