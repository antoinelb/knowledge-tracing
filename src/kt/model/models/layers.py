from functools import partial
from typing import Any, Callable

import jax
import jax.numpy as jnp

#########
# types #
#########

Initializer = Callable[
    [jax.random.KeyArray, tuple[int, ...], Any], jnp.ndarray
]

DenseParams = tuple[jnp.ndarray, jnp.ndarray]
RnnParams = tuple[jnp.ndarray, jnp.ndarray, jnp.ndarray]

############
# external #
############


def create_dense(
    init_weight: Initializer = jax.nn.initializers.normal(stddev=1e-3),
    init_bias: Initializer = jax.nn.initializers.constant(0.0),
) -> tuple[
    Callable[[jax.random.KeyArray, int, int], DenseParams],
    Callable[[DenseParams, jnp.ndarray], jnp.ndarray],
]:
    def init(key: jax.random.KeyArray, n_in: int, n_out: int) -> DenseParams:
        weight_key, bias_key = jax.random.split(key, 2)
        return (
            init_weight(weight_key, (n_in, n_out), jnp.float_),
            init_bias(bias_key, (n_out,), jnp.float_),
        )

    def fct(params: DenseParams, inputs: jnp.ndarray) -> jnp.ndarray:
        weights, bias = params
        return jnp.dot(inputs, weights) + bias

    return init, fct


def create_rnn(
    act_fct: Callable[[jnp.ndarray], jnp.ndarray] = jax.nn.sigmoid,
    init_weight: Initializer = jax.nn.initializers.normal(stddev=1e-3),
    init_bias: Initializer = jax.nn.initializers.constant(0.0),
    init_state: Initializer = jax.nn.initializers.constant(0.0),
) -> tuple[
    Callable[
        [jax.random.KeyArray, int, int],
        RnnParams,
    ],
    Callable[
        [
            RnnParams,
            jax.random.KeyArray,
            jnp.ndarray,
        ],
        jnp.ndarray,
    ],
]:
    def init(key: jax.random.KeyArray, n_in: int, n_out: int) -> RnnParams:
        input_weight_key, hidden_weight_key, bias_key = jax.random.split(
            key, 3
        )
        return (
            init_weight(input_weight_key, (n_in, n_out), jnp.float_),
            init_weight(hidden_weight_key, (n_out, n_out), jnp.float_),
            init_bias(bias_key, (n_out,), jnp.float_),
        )

    def fct(
        params: RnnParams,
        key: jax.random.KeyArray,
        inputs: jnp.ndarray,
    ) -> jnp.ndarray:
        state = init_state(key, (params[-1].shape[0],), jnp.float_)
        rnn_step = partial(_rnn_step, params, act_fct)
        _, outputs = jax.lax.scan(rnn_step, state, inputs)
        return outputs

    return init, fct


############
# internal #
############


def _rnn_step(
    params: tuple[jnp.ndarray, jnp.ndarray, jnp.ndarray],
    act_fct: Callable[[jnp.ndarray], jnp.ndarray],
    state: jnp.ndarray,
    inputs: jnp.ndarray,
) -> tuple[jnp.ndarray, jnp.ndarray]:
    input_weights, hidden_weights, bias = params
    logits = (
        jnp.dot(inputs, input_weights) + jnp.dot(state, hidden_weights) + bias
    )
    outputs = act_fct(logits)
    return outputs, outputs
