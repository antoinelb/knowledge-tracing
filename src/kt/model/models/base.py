from typing import Any, Callable, Literal

import jax
import jax.numpy as jnp
from pydantic import BaseModel
from ruamel.yaml.comments import CommentedMap

from kt.utils.validators import hash_config, set_seed_if_missing

from .utils import MODELS, default_model, get_module

#########
# types #
#########


class Config(BaseModel):
    type: Literal[tuple(MODELS)] = default_model  # type: ignore
    seed: int = 0
    hash: str = ""

    _set_seed = set_seed_if_missing("seed")
    _hash_config = hash_config()


Params = tuple[tuple[jnp.ndarray, ...] | list[tuple[jnp.ndarray, ...]], ...]

InitFct = Callable[[jax.random.KeyArray, int], Params]
"""
Parameters
----------
key : jax.random.KeyArray
    Random key
n_questions : int
    Number of questions in the dataset

Returns
-------
Params
    Initialized parameters
"""

PredictFct = Callable[
    [
        Params,
        jax.random.KeyArray,
        dict[str, jnp.ndarray],
        bool,
    ],
    dict[str, jnp.ndarray],
]
"""
Parameters
----------
params : Params
    Parameters of the model
key : jax.random.KeyArray
    Random key
data : dict[str, jnp.ndarray]
    Dictionary of data containing at minimum "questions" and "answers" with
    some models needing more
train : bool
    If predictions are done during training or inference

Returns
-------
dict[str, jnp.ndarray]
    Predictions of the model with at minimum "predictions"
"""


LossFct = Callable[
    [
        PredictFct,
        Params,
        jax.random.KeyArray,
        dict[str, jnp.ndarray],
        bool,
    ],
    jnp.ndarray,
]
"""
Parameters
----------
predict : PredictFct
    Prediction function to use
params : Params
    Parameters of the model
key : jax.random.KeyArray
    Random key
data : dict[str, jnp.ndarray]
    Dictionary of data containing at minimum "questions" and "answers" with
    some models needing more
train : bool
    If predictions are done during training or inference

Returns
-------
jnp.ndarray
    Calculated loss
"""

ModelFcts = tuple[InitFct, PredictFct, LossFct]

############
# external #
############


def parse_config(config: dict[str, Any] | BaseModel | None = None) -> Config:
    if config is None:
        config = {}
    elif isinstance(config, BaseModel):
        config = config.dict()
    module = get_module(config.get("type", default_model))
    try:
        return module.Config(**config)  # type: ignore
    except AttributeError as exc:
        raise NotImplementedError() from exc


def add_comments(config: Config) -> CommentedMap:
    module = get_module(config.type)
    try:
        config_ = module.add_comments(config)  # type: ignore
    except AttributeError as exc:
        raise NotImplementedError() from exc
    config_.yaml_add_eol_comment(
        f"type of model ({', '.join(MODELS)})",
        "type",
    )
    config_.yaml_add_eol_comment(
        "random seed to use (set to 0 to have new seed)", "seed"
    )
    config_.yaml_add_eol_comment(
        "hash of the config (automatically modified)", "hash"
    )
    return config_


def create_model(config: Config) -> ModelFcts:
    module = get_module(config.type)
    try:
        return module.create_model(config)  # type: ignore
    except AttributeError as exc:
        raise NotImplementedError() from exc
