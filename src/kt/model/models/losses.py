from typing import cast

import jax
import jax.numpy as jnp

from .base import Params, PredictFct


def compute_binary_cross_entropy(
    predict: PredictFct,
    params: Params,
    key: jax.random.KeyArray,
    inputs: dict[str, jnp.ndarray],
    train: bool,
) -> jnp.ndarray:
    """
    Computes the binary cross entropy loss.

    Parameters
    ----------
    predict : PredictFct
        Function to use to predict from inputs
    params : Params
        Parameters of the model
    key : jax.random.KeyArray
        Random key (not used)
    inputs : dict[str, jnp.ndarray]
        Inputs to pass to the model. Most contain at minimum "questions" and
        "answers"
    train : bool
        If this is during training

    Returns
    -------
    jnp.ndarray
        loss
    """
    predictions = cast(
        jnp.ndarray, predict(params, key, inputs, train)["predictions"]
    )
    labels = inputs["answers"]
    if predictions.ndim == 1:
        # make work as if in a batch
        predictions = predictions.reshape(1, -1)
        labels = labels.reshape(1, -1)
        questions = inputs["questions"].reshape(1, -1)
    else:
        # flatten all sequences in the batch
        predictions = predictions.reshape(predictions.shape[0], -1)
        labels = labels.reshape(labels.shape[0], -1)
        questions = inputs["questions"].reshape(
            inputs["questions"].shape[0], -1
        )
    # remove padded questions
    mask = questions != 0
    return -jnp.mean(
        jnp.sum(
            (
                jnp.log(predictions) * labels
                + jnp.log(1 - predictions) * (1 - labels)
            )
            * mask,
            -1,
        )
    )
