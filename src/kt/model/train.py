import json
from functools import partial
from pathlib import Path
from typing import Callable, NamedTuple

import jax
import jax.numpy as jnp
import joblib
import optax
import plotly.graph_objects as go
import tqdm
from pydantic import BaseModel
from ruamel.yaml import CommentedMap

from kt.data import Data
from kt.utils.jax import read_tree, save_tree
from kt.utils.metrics import Metric, MetricFct, get_metric_fct
from kt.utils.print import load_progress
from kt.utils.validators import hash_config, set_seed_if_missing

from .models import InitFct, LossFct, Params, PredictFct

#########
# types #
#########


class Config(BaseModel):
    batch_size: int = 32
    max_epoch: int = 5
    learning_rate: float = 5e-3
    metrics: list[Metric] = ["accuracy"]
    seed: int = 0
    hash: str = ""

    _set_seed = set_seed_if_missing("seed")
    _hash_config = hash_config()


class TrainState(NamedTuple):
    params: Params
    opt_state: optax.OptState
    train_results: list[dict[str, float]]
    valid_results: list[dict[str, float]]


############
# external #
############


def add_comments(config: Config) -> CommentedMap:
    config_ = CommentedMap(config.dict())
    config_.yaml_add_eol_comment("size of each minibatch", "batch_size")
    config_.yaml_add_eol_comment(
        "maximum number of epochs to train for", "max_epoch"
    )
    config_.yaml_add_eol_comment("initial learning rate", "learning_rate")
    config_.yaml_add_eol_comment(
        "metrics to calculate",
        "metrics",
    )
    config_.yaml_add_eol_comment(
        "random seed to use (set to 0 to have new seed)", "seed"
    )
    config_.yaml_add_eol_comment(
        "hash of the config (automatically modified)", "hash"
    )
    return config_


def train(
    config: Config,
    train_data: Data,
    valid_data: Data,
    init: InitFct,
    predict: PredictFct,
    compute_loss: LossFct,
    model_path: Path,
    train_state: TrainState | None = None,
    echo: bool = True,
) -> tuple[Params, dict[str, list[dict[str, float]]]]:
    init_key, *epoch_keys = jax.random.split(
        jax.random.PRNGKey(config.seed), config.max_epoch + 1
    )
    metrics = [(metric, get_metric_fct(metric)) for metric in config.metrics]
    predict = jax.vmap(predict, [None, None, 0, None], 0)

    optimizer = optax.adam(config.learning_rate)

    update = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        config.batch_size,
        metrics,
        train=True,
    )
    evaluate = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        config.batch_size,
        metrics,
        train=False,
    )

    if train_state is None:
        params = init(init_key, train_data.n_questions)
        opt_state = optimizer.init(params)
        train_results: list[dict[str, float]] = []
        valid_results: list[dict[str, float]] = []
    else:
        params, opt_state, train_results, valid_results = train_state

    progress = load_progress(
        epoch_keys, "Training", total=config.max_epoch, echo=echo
    )

    for epoch, key in enumerate(progress):
        if is_train_done(
            TrainState(params, opt_state, train_results, valid_results), config
        ):
            break

        if epoch >= len(valid_results):

            train_key, valid_key = jax.random.split(key)

            params, opt_state, train_results_ = _run_epoch(
                update,
                params,
                train_key,
                train_data,
                opt_state,
                train=True,
                echo=echo,
            )
            train_results = [*train_results, train_results_]

            _, _, valid_results_ = _run_epoch(
                evaluate,
                params,
                train_key,
                valid_data,
                opt_state,
                train=False,
                echo=echo,
            )
            valid_results = [*valid_results, valid_results_]

            _save_epoch(
                params,
                opt_state,
                train_results_,
                valid_results_,
                model_path / str(epoch),
                best=(len(valid_results) == 1)
                or (
                    valid_results_["loss"]
                    < min(res["loss"] for res in valid_results[:-1])
                ),
            )

        if isinstance(progress, tqdm.std.tqdm):
            progress.set_description(
                "[*] Training: "
                + ", ".join(
                    f"t_{metric[0]}: {val:.3f}"
                    for metric, val in train_results[-1].items()
                )
                + ", "
                + ", ".join(
                    f"v_{metric[0]}: {val:.3f}"
                    for metric, val in valid_results[-1].items()
                ),
                refresh=True,
            )
            progress.update()

    train_state = TrainState(params, opt_state, train_results, valid_results)

    params = read_best(model_path)

    return params, {"train": train_results, "valid": valid_results}


def is_train_done(train_state: TrainState | None, config: Config) -> bool:
    if train_state is None:
        return False

    if len(train_state.valid_results) >= config.max_epoch:
        return True
    else:
        return False


def read_train_state(path: Path) -> TrainState | None:
    epochs = sorted(
        [epoch for epoch in path.glob("*") if epoch.stem.isdigit()],
        key=lambda x: int(x.stem),
    )
    train_results: list[dict[str, float]] = []
    valid_results: list[dict[str, float]] = []
    if epochs:
        for epoch in epochs:
            (
                params,
                opt_state,
                train_results_,
                valid_results_,
            ) = _read_epoch(epoch)
            train_results = [*train_results, train_results_]
            valid_results = [*valid_results, valid_results_]
        return TrainState(params, opt_state, train_results, valid_results)
    else:
        return None


def read_best(
    path: Path,
) -> Params:
    with open(path / "best") as f:
        best = int(f.read().strip())
    path = path / str(best)
    params = read_tree(path / "params")
    return params  # type: ignore


def read_results(path: Path) -> dict[str, list[dict[str, float]]]:
    epochs = sorted(
        [epoch for epoch in path.glob("*") if epoch.stem.isdigit()],
        key=lambda x: int(x.stem),
    )
    results: dict[str, list[dict[str, float]]] = {"train": [], "valid": []}
    for epoch in epochs:
        results_ = _read_epoch_results(epoch)
        results = {
            "train": [*results["train"], results_["train"]],
            "valid": [*results["valid"], results_["valid"]],
        }
    return results


def plot_training_curves(
    config: Config, results: dict[str, list[dict[str, float]]]
) -> dict[str, go.Figure]:
    return {
        metric: go.Figure(
            [
                go.Scatter(
                    x=list(range(len(results["train"]))),
                    y=[res[metric] for res in results["train"]],
                    name="train",
                ),
                go.Scatter(
                    x=list(range(len(results["valid"]))),
                    y=[res[metric] for res in results["valid"]],
                    name="valid",
                ),
            ],
            {
                "title": {
                    "text": f"{metric.title()} training curves",
                    "xanchor": "center",
                    "x": 0.5,
                },
                "xaxis_title": "Epoch",
                "yaxis_title": metric.title(),
            },
        )
        for metric in ["loss", *config.metrics]
    }


############
# internal #
############


def _create_run_batch_fct(
    predict: PredictFct,
    compute_loss: LossFct,
    optimizer: optax.GradientTransformation,
    batch_size: int,
    metrics: list[tuple[Metric, MetricFct]],
    train: bool,
) -> Callable[
    [Params, jax.random.KeyArray, dict[str, jnp.ndarray], optax.OptState],
    tuple[
        Params,
        optax.OptState,
        dict[str, jnp.ndarray],
        dict[str, jnp.ndarray],
    ],
]:
    compute_loss_ = partial(compute_loss, predict, train=True)

    def run_batch(
        params: Params,
        key: jax.random.KeyArray,
        inputs: dict[str, jnp.ndarray],
        opt_state: optax.OptState,
    ) -> tuple[
        Params,
        optax.OptState,
        dict[str, jnp.ndarray],
        dict[str, jnp.ndarray],
    ]:
        predictions = predict(params, key, inputs, train)

        if train:
            loss, grads = jax.value_and_grad(compute_loss_)(
                params, key, inputs
            )
            updates, opt_state = optimizer.update(grads, opt_state)
            params = optax.apply_updates(params, updates)
        else:
            loss = compute_loss_(params, key, inputs)

        results = {
            "loss": loss,
            **{
                metric: metric_fct(inputs | predictions)
                for metric, metric_fct in metrics
            },
        }

        return params, opt_state, results, inputs | predictions

    return jax.jit(run_batch)


def _run_epoch(
    fct: Callable[
        [
            Params,
            jax.random.KeyArray,
            dict[str, jnp.ndarray],
            optax.OptState,
        ],
        tuple[
            Params,
            optax.OptState,
            dict[str, jnp.ndarray],
            dict[str, jnp.ndarray],
        ],
    ],
    params: Params,
    key: jax.random.KeyArray,
    data: Data,
    opt_state: optax.OptState,
    train: bool,
    echo: bool,
) -> tuple[Params, optax.OptState, dict[str, float]]:
    keys = jax.random.split(key, len(data))
    results: dict[str, float] = {}

    progress = load_progress(
        zip(data, keys),
        "  Train: " if train else "  Valid: ",
        total=len(data),
        echo=echo,
    )

    for batch, (inputs, key) in enumerate(progress):
        params, opt_state, results_, predictions = fct(
            params, key, inputs, opt_state
        )
        results = {
            metric: (
                (
                    results.get(metric, 0) * batch * data.batch_size
                    + val.item() * inputs["questions"].shape[0]
                )
                / (batch * data.batch_size + inputs["questions"].shape[0])
            )
            for metric, val in results_.items()
        }
        if isinstance(progress, tqdm.std.tqdm):
            progress.set_description(  # type: ignore
                f"[*]   {'Train' if train else 'Valid'}: "
                + ", ".join(
                    f"{metric[0]}: {val:.3f}"
                    for metric, val in results.items()
                ),
                refresh=True,
            )

    return params, opt_state, results


def _save_epoch(
    params: Params,
    opt_state: optax.OptState,
    train_results: dict[str, float],
    valid_results: dict[str, float],
    path: Path,
    best: bool,
) -> None:
    path.mkdir(parents=True, exist_ok=True)

    save_tree(params, path / "params")
    joblib.dump(opt_state, path / "train_state.joblib")

    with open(path / "train_results.json", "w") as f:
        json.dump(train_results, f)
    with open(path / "valid_results.json", "w") as f:
        json.dump(valid_results, f)

    if best:
        with open(path / ".." / "best", "w") as f:
            f.write(path.stem)


def _read_epoch(
    path: Path,
) -> tuple[Params, optax.OptState, dict[str, float], dict[str, float]]:
    params = read_tree(path / "params")
    opt_state = joblib.load(path / "train_state.joblib")

    with open(path / "train_results.json", "r") as f:
        train_results = json.load(f)
    with open(path / "valid_results.json", "r") as f:
        valid_results = json.load(f)

    return (  # type: ignore
        params,
        opt_state,
        train_results,
        valid_results,
    )


def _read_epoch_results(path: Path) -> dict[str, dict[str, float]]:
    with open(path / "train_results.json", "r") as f:
        train_results = json.load(f)
    with open(path / "valid_results.json", "r") as f:
        valid_results = json.load(f)
    return {"train": train_results, "valid": valid_results}
