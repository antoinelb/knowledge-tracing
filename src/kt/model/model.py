import json
import time
from pathlib import Path
from typing import Callable

import jax
import jax.numpy as jnp
import plotly.graph_objects as go
import polars as pl
from pydantic import BaseModel
from ruamel.yaml.comments import CommentedMap

from kt import data as data_
from kt.utils.print import done_print, load_print
from kt.utils.validators import hash_config, parse_config

from . import models
from . import train as train_

#########
# types #
#########


class Config(BaseModel):
    data: data_.Config = data_.parse_config()
    model: models.Config = models.parse_config()
    train: train_.Config = train_.Config()
    hash: str = ""

    _parse_data = parse_config("data", data_.parse_config)
    _parse_model = parse_config("model", models.parse_config)
    _hash_config = hash_config()


# key: jax.random.KeyArray,
# inputs: dict[str, jnp.ndarray]
ModelFct = Callable[
    [jax.random.KeyArray, dict[str, jnp.ndarray]],
    dict[str, jnp.ndarray],
]


############
# external #
############


def add_comments(config: Config) -> CommentedMap:
    config_ = CommentedMap(config.dict())
    config_["data"] = data_.add_comments(config.data)
    config_["model"] = models.add_comments(config.model)
    config_["train"] = train_.add_comments(config.train)
    config_.yaml_add_eol_comment(
        "hash of the config (automatically modified)", "hash"
    )
    return config_


def get_model(
    config: Config,
    data_dir: Path,
    model_dir: Path,
    cache_dir: Path,
    force_training: bool = False,
    echo: bool = True,
) -> tuple[
    ModelFct,
    dict[int, int],
    dict[str, dict[str, list[dict[str, float]]] | dict[str, go.Figure]],
]:
    """
    Creates and returns the model, training it if it hadn't been before.

    Parameters
    ----------
    config : Config
        Configuration to use to create and train model
    data_dir : Path
        Path where the data is saved
    model_dir : Path
        Path where to save the models
    cache_dir : Path
        Path where to save the caches
    force_training : bool (default : False)
        If the model should be retrained
    echo : bool (default : True)
        If steps should be echoed to console

    Returns
    -------
    ModelFct
        Trained model predict function
    dict[int, int]
        Dictionary to convert questions to the right indices
    dict[str, list[dict[str, float]]]
        Train results in the format:
            {
                "train": [
                    {
                        metric: value
                    }
                ],
                "valid": [
                    {
                        metric: value
                    }
                ],
            }
    """
    model_path = model_dir / config.data.type / config.model.type / config.hash

    init, predict, compute_loss = models.create_model(config.model)

    if force_training:
        model_state = None
    else:
        model_state = _read_model_state(model_path, echo=echo)

    if model_state is not None:
        question2index, train_state = model_state
    else:
        question2index, train_state = {}, None

    if train_.is_train_done(train_state, config.train):
        params = train_.read_best(model_path)
        results = train_.read_results(model_path)
        done_print("Read trained model.", echo=echo)

    else:
        train_data, valid_data, question2index = _read_data(
            config.data,
            config.train.batch_size,
            data_dir,
            cache_dir,
            echo=echo,
        )
        _save_question2index(question2index, model_path)

        params, results = train_.train(
            config.train,
            train_data,
            valid_data,
            init,
            predict,
            compute_loss,
            model_path,
            train_state,
            echo=echo,
        )

        done_print("Trained model.", echo=echo)

    plots = train_.plot_training_curves(config.train, results)

    return (
        _create_model_fct(predict, params),
        question2index,
        {"results": results, "plots": plots},
    )


############
# internal #
############


def _create_model_fct(
    predict: models.PredictFct, params: models.Params
) -> ModelFct:
    def model_fct(
        key: jax.random.KeyArray, inputs: dict[str, jnp.ndarray]
    ) -> dict[str, jnp.ndarray]:
        start = time.time()
        predictions = predict(params, key, inputs, False)
        return {**predictions, "time": jnp.array(time.time() - start)}

    return model_fct


def _read_data(
    config: data_.Config,
    batch_size: int,
    data_dir: Path,
    cache_dir: Path,
    echo: bool,
) -> tuple[data_.Data, data_.Data, dict[int, int]]:
    data = data_.read_data(config, data_dir, cache_dir, echo=echo)
    train_data, question2index = data_.convert_data(
        data.filter(pl.col("dataset") == "train"),
        config,
        batch_size,
        cache_dir,
        echo=echo,
    )
    valid_data, _ = data_.convert_data(
        data.filter(pl.col("dataset") == "valid"),
        config,
        batch_size,
        cache_dir,
        question2index=question2index,
        echo=echo,
    )
    return train_data, valid_data, question2index


def _read_model_state(
    path: Path, echo: bool = True
) -> tuple[dict[int, int], train_.TrainState] | None:
    try:
        with open(path / "question2index.json") as f:
            question2index_ = json.load(f)
        question2index = {int(q): i for q, i in question2index_.items()}
    except FileNotFoundError:
        return None

    load_print("Reading previous training state...", echo=echo)
    train_state = train_.read_train_state(path)
    if train_state is None:
        return None

    return question2index, train_state


def _save_question2index(question2index: dict[int, int], path: Path) -> None:
    path.mkdir(parents=True, exist_ok=True)
    with open(path / "question2index.json", "w") as f:
        json.dump(question2index, f)
