import warnings

warnings.filterwarnings("ignore", category=FutureWarning)

import typer

from kt.utils.paths import determine_path
from kt.utils.print import done_print

from .experiment import Config, read_config, run_experiment, save_config


def init_cli() -> typer.Typer:
    cli = typer.Typer(context_settings={"help_option_names": ["-h", "--help"]})
    cli.command("create")(create_or_update)
    cli.command("c", hidden=True)(create_or_update)
    cli.command("run")(run)
    cli.command("r", hidden=True)(run)
    return cli


def run_cli() -> None:
    cli = init_cli()
    cli()


def create_or_update(
    file: str = typer.Argument(
        ...,
        help="File to create or update (a .yml extension will be added "
        "if missing)",
    )
) -> None:
    """
    (c) Creates or updates an experiment config file with defaults.
    """
    if not file.endswith(".yml"):
        file = f"{file}.yml"
    path = determine_path(file)
    try:
        config = read_config(path)
        save_config(config, path)
        done_print(f"Updated config {file}.")
    except FileNotFoundError:
        config = Config(name=path.stem)
        save_config(config, path)
        done_print(f"Created config {file}.")


def run(
    experiments: list[str] = typer.Argument(..., help="experiments to run"),
    train: bool = typer.Option(
        False,
        "-t",
        "--train",
        help="If training should be restarted even if saved state exists",
    ),
    eval_: bool = typer.Option(
        False,
        "-e",
        "--eval",
        help="If evaluation should be restarted even if saved results exists",
    ),
    data_dir: str = typer.Option(
        "data", "--data-dir", help="Directory where to save the raw data"
    ),
    model_dir: str = typer.Option(
        "models",
        "--model-dir",
        help="Directory where to save the trained models",
    ),
    results_dir: str = typer.Option(
        "results",
        "--results-dir",
        help="Directory where to save the results",
    ),
    mlflow_uri: str = typer.Option(
        ".mlruns",
        "--mlflow-uri",
        help="Where to save mlflow results",
    ),
    cache_dir: str = typer.Option(
        ".cache", "--cache-dir", help="Directory where to save the caches"
    ),
) -> None:
    """
    (r) Runs the experiment(s).
    """
    configs = [
        read_config(determine_path(experiment)) for experiment in experiments
    ]
    data_dir_ = determine_path(data_dir)
    model_dir_ = determine_path(model_dir)
    results_dir_ = determine_path(results_dir)
    cache_dir_ = determine_path(cache_dir)
    for config in configs:
        run_experiment(
            config,
            data_dir_,
            model_dir_,
            results_dir_,
            cache_dir_,
            mlflow_uri,
            force_training=train,
            force_eval=eval_,
        )
