import json
from pathlib import Path
from typing import Literal, get_args

import jax
import jax.numpy as jnp
import polars as pl
import tqdm
from pydantic import BaseModel
from ruamel.yaml.comments import CommentedMap

from kt import data as data_
from kt.model import ModelFct
from kt.utils.analysis import convert_predictions
from kt.utils.metrics import Metric, MetricFct, get_metric_fct
from kt.utils.print import done_print, load_progress
from kt.utils.validators import hash_config, parse_config, set_seed_if_missing

#########
# types #
#########

Split = Literal["train", "valid", "test"]


class Config(BaseModel):
    data: data_.Config = data_.parse_config()
    splits: list[Split] = ["test"]
    metrics: list[Metric] = ["accuracy", "auc", "mean_time"]
    batch_size: int = 32
    seed: int = 0
    hash: str = ""

    _parse_data = parse_config("data", data_.parse_config)
    _set_seed = set_seed_if_missing("seed")
    _hash_config = hash_config(ignore=["splits"])


############
# external #
############


def add_comments(config: Config) -> CommentedMap:
    config_ = CommentedMap(config.dict())
    config_["data"] = data_.add_comments(config.data)
    config_.yaml_add_eol_comment(
        "on which splits to run the evaluation "
        + f"({', '.join(get_args(Split))})",
        "splits",
    )
    config_.yaml_add_eol_comment("metrics to calculate", "metrics")
    config_.yaml_add_eol_comment("size of each minibatch", "batch_size")
    config_.yaml_add_eol_comment(
        "random seed to use (set to 0 to have new seed)", "seed"
    )
    config_.yaml_add_eol_comment(
        "hash of the config (automatically modified)", "hash"
    )
    return config_


def evaluate(
    model: ModelFct,
    question2index: dict[int, int],
    config: Config,
    data_dir: Path,
    results_path: Path,
    cache_dir: Path,
    force_eval: bool = False,
    echo: bool = True,
) -> dict[Split, dict[str, float]]:
    data = _read_data(
        config.data,
        config.splits,
        question2index,
        config.batch_size,
        data_dir,
        cache_dir,
        echo=echo,
    )

    metrics = [(metric, get_metric_fct(metric)) for metric in config.metrics]
    model = jax.vmap(model, [None, 0], 0)

    if force_eval:
        results: dict[Split, dict[str, float]] = {}
        predictions: dict[Split, pl.DataFrame] = {}
    else:
        results, predictions = read_results(results_path)
    for split, _data in data.items():
        if split not in results or split not in predictions:
            results[split], predictions[split] = _evaluate(
                model,
                jax.random.PRNGKey(config.seed),
                split,
                _data,
                metrics,
                echo=echo,
            )

    _save_results(results, predictions, results_path)

    done_print("Evaluation done.", echo=echo)

    return results


def read_results(
    path: Path,
) -> tuple[dict[Split, dict[str, float]], dict[Split, pl.DataFrame]]:
    results: dict[Split, dict[str, float]] = {}
    predictions: dict[Split, pl.DataFrame] = {}
    for split in get_args(Split):
        path_ = path / split
        if (path_ / "results.json").exists() and (
            path_ / "predictions.ipc"
        ).exists():
            with open(path_ / "results.json") as f:
                results[split] = json.load(f)
            predictions[split] = pl.read_ipc(path_ / "predictions.ipc")
    return results, predictions


############
# internal #
############


def _read_data(
    config: data_.Config,
    splits: list[Split],
    question2index: dict[int, int],
    batch_size: int,
    data_dir: Path,
    cache_dir: Path,
    echo: bool,
) -> dict[Split, data_.Data]:
    data = data_.read_data(config, data_dir, cache_dir, echo=echo)
    return {
        split: data_.convert_data(
            data.filter(pl.col("dataset") == split),
            config,
            batch_size,
            cache_dir,
            question2index=question2index,
            echo=echo,
        )[0]
        for split in set(splits)
    }


def _evaluate(
    model: ModelFct,
    key: jax.random.KeyArray,
    split: Literal["train", "valid", "test"],
    data: data_.Data,
    metrics: list[tuple[Metric, MetricFct]],
    echo: bool,
) -> tuple[dict[str, float], pl.DataFrame]:
    keys = jax.random.split(key, len(data))
    results: dict[str, float] = {}
    predictions: list[dict[str, jnp.ndarray]] = []

    time_metrics = [
        (metric, fct) for metric, fct in metrics if "time" in metric
    ]
    metrics = [
        (metric, fct) for metric, fct in metrics if "time" not in metric
    ]

    progress = load_progress(
        zip(data, keys),
        f"Evaluating {split}: ",
        total=len(data),
        echo=echo,
    )

    for batch, (inputs, key) in enumerate(progress):
        predictions_ = model(key, inputs)
        results = {
            metric: (
                (
                    results.get(metric, 0) * batch * data.batch_size
                    + metric_fct(inputs | predictions_).item()
                    * inputs["questions"].shape[0]
                )
                / (batch * data.batch_size + inputs["questions"].shape[0])
            )
            for metric, metric_fct in metrics
        }
        predictions.append(inputs | predictions_)
        if isinstance(progress, tqdm.std.tqdm):
            progress.set_description(  # type: ignore
                f"[*] Evaluating {split}: "
                + ", ".join(
                    f"{metric}: {val:.3f}" for metric, val in results.items()
                ),
                refresh=True,
            )
    results = {
        **results,
        **{
            metric: fct(
                {
                    key: jnp.concatenate([preds[key] for preds in predictions])
                    for key in predictions[0].keys()
                }
            ).item()
            for metric, fct in time_metrics
        },
    }

    return results, convert_predictions(predictions)


def _save_results(
    results: dict[Split, dict[str, float]],
    predictions: dict[Split, pl.DataFrame],
    path: Path,
) -> None:
    for split in results.keys():
        path_ = path / split
        path_.mkdir(parents=True, exist_ok=True)
        with open(path_ / "results.json", "w") as f:
            json.dump(results[split], f, indent=2)
        # overwriting the path sometimes creates bus errors
        if (predictions_path := path_ / "predictions.ipc").exists():
            predictions_path.unlink()
        predictions[split].write_ipc(path_ / "predictions.ipc")
