import jax.numpy as jnp
import polars as pl


def convert_predictions(
    predictions: list[dict[str, jnp.ndarray]]
) -> pl.DataFrame:
    return pl.DataFrame(
        {
            key: [
                vals.tolist()
                for predictions_ in predictions
                for vals in predictions_[key]
            ]
            for key in predictions[0].keys()
        }
    )
