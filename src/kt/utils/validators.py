import hashlib
import random
from typing import Any, Callable, Type, Union

from pydantic import BaseModel, root_validator, validator

############
# external #
############


def hash_config(ignore: list[str] = []) -> Callable:
    def fct_(cls: Type[BaseModel], vals: dict[str, Any]) -> dict[str, Any]:
        return {
            **vals,
            "hash": _hash_config(
                {
                    key: val.dict() if isinstance(val, BaseModel) else val
                    for key, val in vals.items()
                    if key not in ignore
                }
            ),
        }

    return root_validator(fct_, allow_reuse=True)  # type: ignore


def parse_config(field: str, fct: Callable) -> Callable:
    def fct_(
        cls: Type[BaseModel], val: Union[dict[str, Any], BaseModel, None]
    ) -> BaseModel:
        if isinstance(val, BaseModel):
            return val
        elif val is None:
            return fct()
        else:
            return fct(val)

    return validator(field, allow_reuse=True, always=True, pre=True)(fct_)  # type: ignore # noqa


def set_field(field: str, value: Any) -> Callable:
    def fct_(cls: Type[BaseModel], val: Any) -> Any:
        return value

    return validator(field, allow_reuse=True, always=True)(fct_)  # type: ignore # noqa


def verify_between_0_and_1(field: str) -> Callable:
    def fct_(cls: Type[BaseModel], val: float) -> float:
        if not 0 <= val <= 1:
            raise ValueError(
                f"The value of `{field}` must be between 0 and 1."
            )
        return val

    return validator(field, allow_reuse=True)(fct_)  # type: ignore


def set_seed_if_missing(field: str) -> Callable:
    def fct_(val: int) -> int:
        if val == 0:
            return random.randint(0, 100_000)  # nosec
        else:
            return val

    return validator(field, allow_reuse=True, always=True)(fct_)  # type: ignore # noqa


############
# internal #
############


def _hash_config(config: dict[str, Any]) -> str:
    config = _prepare_config_for_hashing(config)
    return hashlib.sha256(str(config).encode()).hexdigest()[:8]


def _prepare_config_for_hashing(config: Any) -> Any:
    if isinstance(config, dict):
        return {
            key: _prepare_config_for_hashing(config[key])
            for key in sorted(config)
            if key != "hash"
        }
    elif isinstance(config, list):
        return [_prepare_config_for_hashing(x) for x in config]
    else:
        return config
