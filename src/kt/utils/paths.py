from pathlib import Path

root_dir = Path(__file__).parent / ".." / ".." / ".."
cache_dir = root_dir / ".cache"


def determine_path(path: str) -> Path:
    if path.startswith("~"):
        return Path(path).expanduser()
    elif path.startswith("/"):
        return Path(path)
    elif path.startswith("./"):
        return root_dir / path[2:]
    else:
        return root_dir / path
