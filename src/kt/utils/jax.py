import re
from pathlib import Path
from typing import Any

import jax
import jax.numpy as jnp

############
# external #
############


def save_tree(tree: Any, path: Path) -> None:
    path.mkdir(parents=True, exist_ok=True)
    arrays, structure = _convert_from_tree(tree)
    jnp.savez(path / "arrays.npz", *arrays)
    with open(path / "structure", "w") as f:
        f.write(structure)


def read_tree(path: Path) -> list[Any]:
    arrays_ = jnp.load(path / "arrays.npz")
    arrays = [jnp.array(arrays_[key]) for key in arrays_.files]
    with open(path / "structure") as f:
        structure = f.read().strip()
    return _convert_to_tree(arrays, structure)


def get_shapes(data: Any) -> Any:
    return jax.tree_map(lambda x: x.shape if hasattr(x, "shape") else x, data)


############
# internal #
############


def _convert_from_tree(tree: Any) -> tuple[list[jnp.ndarray], str]:
    arrays, structure_ = jax.tree_util.tree_flatten(tree)
    structure = re.match(r"^PyTreeDef\((.+)\)$", str(structure_)).group(1)  # type: ignore # noqa
    return arrays, structure


def _convert_to_tree(arrays: list[jnp.ndarray], structure: str) -> Any:
    structure = structure.replace(" ", "")

    if structure[0] == "*":
        return arrays[0]

    elif structure[0] == "[":
        nest = 0
        list_: list[Any] = []
        current_arrays: list[jnp.ndarray] = []
        current_structure = ""
        for c in structure[1:-1]:
            if c == "*":
                current_arrays.append(arrays[0])
                arrays = arrays[1:]
                current_structure += c
            elif c in ("[", "(", "{"):
                nest = nest + 1
                current_structure += c
            elif c in ("]", ")", "}"):
                nest = nest - 1
                current_structure += c
            elif c == ",":
                if nest == 0:
                    list_.append(
                        _convert_to_tree(current_arrays, current_structure)
                    )
                    current_arrays = []
                    current_structure = ""
                else:
                    current_structure += c
            else:
                current_structure += c
        list_.append(_convert_to_tree(current_arrays, current_structure))
        return list_
    elif structure[0] == "(":
        nest = 0
        tuple_: tuple[Any, ...] = ()
        current_arrays = []
        current_structure = ""
        for c in structure[1:-1]:
            if c == "*":
                current_arrays.append(arrays[0])
                arrays = arrays[1:]
                current_structure += c
            elif c in ("[", "(", "{"):
                nest = nest + 1
                current_structure += c
            elif c in ("]", ")", "}"):
                nest = nest - 1
                current_structure += c
            elif c == ",":
                if nest == 0:
                    tuple_ = tuple(
                        [
                            *tuple_,
                            _convert_to_tree(
                                current_arrays, current_structure
                            ),
                        ]
                    )
                    current_arrays = []
                    current_structure = ""
                else:
                    current_structure += c
            else:
                current_structure += c
        tuple_ = tuple(
            [*tuple_, _convert_to_tree(current_arrays, current_structure)]
        )
        return tuple_
    elif structure[0] == "{":
        nest = 0
        dict_: dict[str, Any] = {}
        current_arrays = []
        current_structure = ""
        current_key = ""
        in_key = True
        for c in structure[1:-1]:
            if c == "*":
                current_arrays.append(arrays[0])
                arrays = arrays[1:]
                current_structure += c
            elif c in ("[", "(", "{"):
                nest = nest + 1
                current_structure += c
            elif c in ("]", ")", "}"):
                nest = nest - 1
                current_structure += c
            elif c == ",":
                if nest == 0:
                    dict_[current_key] = _convert_to_tree(
                        current_arrays, current_structure
                    )
                    current_arrays = []
                    current_structure = ""
                    current_key = ""
                    in_key = True
                else:
                    current_structure += c
            elif c == ":":
                in_key = False
            else:
                if in_key:
                    if c != "'":
                        current_key += c
                else:
                    current_structure += c
        dict_[current_key] = _convert_to_tree(
            current_arrays, current_structure
        )
        return dict_
    else:
        raise NotImplementedError(
            f"The structure starting with {structure[0]} can't be handled."
        )
