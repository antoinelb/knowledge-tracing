import shutil
from typing import Any, Iterable

from tqdm import tqdm


def load_print(text: str, symbol: str = "*", echo: bool = True) -> None:
    symbol = f"\033[1m[{symbol}]\033[0m"
    if echo:
        print(
            f"\r{symbol} {text}".ljust(shutil.get_terminal_size().columns),
            end="\r",
        )


def done_print(text: str, symbol: str = "+", echo: bool = True) -> None:
    symbol = f"\033[1m\033[92m[{symbol}]\033[0m"
    if echo:
        print(f"\r{symbol} {text}".ljust(shutil.get_terminal_size().columns))


def warning_print(text: str, symbol: str = "!", echo: bool = True) -> None:
    symbol = f"\033[1m\033[93m[{symbol}]\033[0m"
    if echo:
        print(f"\r{symbol} {text}".ljust(shutil.get_terminal_size().columns))


def load_progress(
    iter_: Iterable[Any],
    text: str,
    symbol: str = "*",
    echo: bool = True,
    *args: Any,
    **kwargs: Any,
) -> Iterable[Any]:
    symbol = f"\033[1m[{symbol}]\033[0m"
    if echo:
        return tqdm(
            iter_,
            f"\r{symbol} {text}",
            *args,
            leave=False,
            **kwargs,
        )
    else:
        return iter_


def format_list(list_: list[str], surround: str = "") -> str:
    if surround:
        list_ = [f"{surround}{x}{surround}" for x in list_]
    if len(list_) == 0:
        return ""
    elif len(list_) == 1:
        return list_[0]
    else:
        return f"{', '.join(list_[:-1])} and {list_[-1]}"


def format_time(time: float) -> str:
    time = round(time)
    seconds = time % 60
    time = time // 60
    minutes = time % 60
    time = time // 60
    hours = time % 60
    time_ = ""
    if hours:
        time_ = f"{time_}{hours}h"
    if minutes or hours:
        time_ = f"{time_}{minutes}m"
    time_ = f"{time_}{seconds}s"
    return time_
