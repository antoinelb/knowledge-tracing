from typing import Callable, Literal, cast

import jax.numpy as jnp
from jax import jit

#########
# types #
#########

Metric = Literal[
    "accuracy",
    "auc",
    "sigmoid_crossentropy",
    "mean_time",
    "std_time",
    "max_time",
]
MetricFct = Callable[[dict[str, jnp.ndarray]], jnp.ndarray]

############
# external #
############


def get_metric_fct(metric: Metric) -> MetricFct:
    if metric == "accuracy":
        return _accuracy
    elif metric == "auc":
        return _auc
    elif metric == "sigmoid_crossentropy":
        return _sigmoid_crossentropy
    elif metric == "mean_time":
        return _mean_time
    elif metric == "std_time":
        return _std_time
    elif metric == "max_time":
        return _max_time
    else:
        raise NotImplementedError()


############
# internal #
############


@jit
def _sigmoid_crossentropy(inputs: dict[str, jnp.ndarray]) -> jnp.ndarray:
    predictions = cast(jnp.ndarray, inputs["predictions"])
    questions = cast(jnp.ndarray, inputs["questions"])
    answers = cast(jnp.ndarray, inputs["answers"])

    predictions = jnp.ravel(predictions)
    questions = jnp.ravel(questions)
    answers = jnp.ravel(answers)

    mask = questions != 0

    # make it so the log of padding returns 0
    return -jnp.sum(
        answers * jnp.log(predictions * mask + jnp.logical_not(mask))
        + (1 - answers) * jnp.log(1 - predictions * mask)
    ) / jnp.sum(mask)


@jit
def _accuracy(inputs: dict[str, jnp.ndarray]) -> jnp.ndarray:
    predictions = cast(jnp.ndarray, inputs["predictions"])
    questions = cast(jnp.ndarray, inputs["questions"])
    answers = cast(jnp.ndarray, inputs["answers"])

    predictions = jnp.ravel(predictions)
    questions = jnp.ravel(questions)
    answers = jnp.ravel(answers)

    mask = questions != 0

    predictions = jnp.round(predictions)

    return jnp.sum((predictions == answers) * mask) / jnp.sum(mask)


def _auc(inputs: dict[str, jnp.ndarray]) -> jnp.ndarray:
    true_positive_rate, false_positive_rate, _ = _roc_curve(inputs)
    return jnp.trapz(true_positive_rate, false_positive_rate)


def _roc_curve(
    inputs: dict[str, jnp.ndarray]
) -> tuple[jnp.ndarray, jnp.ndarray, jnp.ndarray]:
    predictions = cast(jnp.ndarray, inputs["predictions"])
    questions = cast(jnp.ndarray, inputs["questions"])
    answers = cast(jnp.ndarray, inputs["answers"])

    predictions = jnp.ravel(predictions)
    questions = jnp.ravel(questions)
    answers = jnp.ravel(answers)

    mask = questions != 0

    predictions = predictions[mask]
    answers = answers[mask]

    thresholds = jnp.array(range(11)) / 10

    indices = jnp.argsort(predictions)[::-1]
    predictions = predictions[indices]
    answers = answers[indices]

    distinct_value_indices = jnp.arange(predictions.size)[
        jnp.r_[jnp.diff(predictions) != 0, False]
    ]
    threshold_indices = jnp.r_[distinct_value_indices, answers.size - 1]

    true_positives = jnp.cumsum(answers)[threshold_indices]
    false_positives = threshold_indices + 1 - true_positives

    true_positive_rate = true_positives / true_positives[-1]
    false_positive_rate = false_positives / false_positives[-1]
    thresholds = predictions[threshold_indices]

    true_positive_rate = jnp.r_[0, true_positive_rate]
    false_positive_rate = jnp.r_[0, false_positive_rate]
    thresholds = jnp.r_[thresholds[0] + 1, thresholds]

    return true_positive_rate, false_positive_rate, thresholds


@jit
def _mean_time(inputs: dict[str, jnp.ndarray]) -> jnp.ndarray:
    time = cast(jnp.ndarray, inputs["time"])
    return jnp.mean(time)


@jit
def _std_time(inputs: dict[str, jnp.ndarray]) -> jnp.ndarray:
    time = cast(jnp.ndarray, inputs["time"])
    return jnp.std(time)


@jit
def _max_time(inputs: dict[str, jnp.ndarray]) -> jnp.ndarray:
    time = cast(jnp.ndarray, inputs["time"])
    return jnp.max(time)
