import json
import tempfile
from pathlib import Path
from typing import Any, Iterator

import mlflow
import plotly.graph_objects as go
from pydantic import BaseModel
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap

from kt.utils.print import done_print, load_print, warning_print
from kt.utils.validators import hash_config

from . import evaluation
from . import model as model_

#########
# types #
#########


class Config(BaseModel):
    name: str
    experiment: str = "Default"
    model: model_.Config = model_.Config()
    eval: evaluation.Config = evaluation.Config()
    hash: str = ""

    _hash_config = hash_config()


############
# external #
############


def run_experiment(
    config: Config,
    data_dir: Path,
    model_dir: Path,
    results_dir: Path,
    cache_dir: Path,
    mlflow_uri: str,
    force_training: bool = False,
    force_eval: bool = False,
    echo: bool = True,
) -> None:
    _init_mlflow(config, mlflow_uri)
    results_path = results_dir / f"{config.name}:{config.hash}"
    model, question2index, train_results = model_.get_model(
        config.model,
        data_dir,
        model_dir,
        cache_dir,
        force_training=force_training,
        echo=echo,
    )
    eval_results = evaluation.evaluate(
        model,
        question2index,
        config.eval,
        data_dir,
        results_path,
        cache_dir,
        force_eval=force_eval,
        echo=echo,
    )

    _log_results_in_mlflow(
        config, train_results, eval_results, echo=echo  # type: ignore
    )

    mlflow.end_run()

    done_print("Experiment done.", echo=echo)


def read_config(path: Path) -> Config:
    yaml = YAML()
    config_ = yaml.load(path)
    config_["name"] = path.stem
    config = Config(**config_)
    if (config.model.data.type == config.eval.data.type) and (
        config.model.data.seed != config.eval.data.seed
    ):
        warning_print(
            "The same type of data was used for training and evaluation, "
            "but with different seeds."
        )
    return config


def save_config(config: Config, path: Path) -> None:
    path.parent.mkdir(parents=True, exist_ok=True)
    config_ = _add_comments(config)
    del config_["name"]
    yaml = YAML()
    yaml.dump(config_, path)


############
# internal #
############


def _add_comments(config: Config) -> CommentedMap:
    config_ = CommentedMap(config.dict())
    config_.yaml_add_eol_comment(
        "name of the experiment in mlflow", "experiment"
    )
    config_["model"] = model_.add_comments(config.model)
    config_["eval"] = evaluation.add_comments(config.eval)
    config_.yaml_add_eol_comment(
        "hash of the config (automatically modified)", "hash"
    )
    return config_


def _init_mlflow(config: Config, mlflow_uri: str) -> None:
    mlflow.set_tracking_uri(mlflow_uri)
    mlflow.set_experiment(config.experiment)
    mlflow.start_run(run_name=config.name)
    mlflow.log_params(
        _flatten_dict(
            {
                key: val
                for key, val in config.dict().items()
                if key not in ("name", "experiment")
            }
        )
    )


def _flatten_dict(dict_: dict[str, Any]) -> dict[str, str | int | float]:
    def flatten(
        _dict: dict[str, Any], key: str = ""
    ) -> Iterator[tuple[str, str | int | float]]:
        key = f"{key}__" if key else ""
        for key_, val in _dict.items():
            key_ = f"{key}{key_}"
            if isinstance(val, dict):
                yield from flatten(val, key_)
            else:
                yield key_, val

    return dict(flatten(dict_))


def _log_results_in_mlflow(
    config: Config,
    train_results: dict[
        str, dict[str, list[dict[str, float]]] | dict[str, go.Figure]
    ],
    eval_results: dict[str, dict[str, float]],
    echo: bool = True,
) -> None:
    load_print("Logging results in mlflow...", echo=echo)

    _log_mlflow_artifact(config.dict(), "config.yml")

    _log_mlflow_artifact(train_results["results"], "train_results.json")
    for metric, plot in train_results["plots"].items():
        mlflow.log_figure(plot, f"{metric}_training_curves.svg")

    mlflow.log_metrics(_flatten_dict(eval_results))  # type: ignore


def _log_mlflow_artifact(data: Any, file_name: str) -> None:
    path = Path(tempfile.mkdtemp()) / file_name
    if path.suffix == ".json":
        with open(path, "w") as f:
            json.dump(data, f)
    elif path.suffix in (".yml", ".yaml"):
        yaml = YAML()
        with open(path, "w") as f:
            yaml.dump(data, f)
    else:
        raise NotImplementedError(
            f"Saving artifact with extension {path.suffix} isn't supperted."
        )
    mlflow.log_artifact(str(path))
