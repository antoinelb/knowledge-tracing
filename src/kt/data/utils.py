import importlib
from pathlib import Path
from types import ModuleType

#########
# types #
#########


DATASETS = [
    file.stem
    for file in (Path(__file__).parent / "datasets").glob("*.py")
    if file.stem != "__init__"
]

default_dataset = "synthetic_5"

############
# external #
############


def get_module(type_: str) -> ModuleType:
    try:
        return importlib.import_module(
            f"kt.data.datasets.{type_}",
        )
    except ModuleNotFoundError as exc:
        raise NotImplementedError(f"There is no data type {type_}.") from exc
