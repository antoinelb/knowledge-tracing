import math
from pathlib import Path
from typing import Any, Iterator, Literal

import jax.numpy as jnp
import numpy as np
import polars as pl
from pydantic import BaseModel
from ruamel.yaml.comments import CommentedMap

from kt.utils.print import done_print, load_print
from kt.utils.validators import (
    hash_config,
    set_seed_if_missing,
    verify_between_0_and_1,
)

from .utils import DATASETS, default_dataset, get_module

#########
# types #
#########


class Config(BaseModel):
    type: Literal[tuple(DATASETS)] = default_dataset  # type: ignore
    user_field: str = "student"
    timestamp_field: str = "timestamp"
    question_field: str = "question"
    answer_field: str = "answer"
    p_valid: float = 0.1
    p_test: float = 0.1
    seed: int = 0
    hash: str = ""

    _valid_p_valid = verify_between_0_and_1("p_valid")
    _test_p_test = verify_between_0_and_1("p_test")
    _set_seed = set_seed_if_missing("seed")
    _hash_config = hash_config()


class Data:
    def __init__(
        self,
        data: pl.DataFrame,
        n_questions: int,
        batch_size: int,
        batch_index: dict[int, int] | None = None,
    ) -> None:
        self._data = data
        self._n_questions = n_questions
        self._batch_size = batch_size
        self._batch_index = (
            {i: i for i in range(data.shape[0])}
            if batch_index is None
            else batch_index
        )

    def __getitem__(self, idx: int) -> dict[str, jnp.ndarray]:
        i = self._batch_index[idx]
        return _create_batch(
            self._data[i * self._batch_size : (i + 1) * self._batch_size]
        )

    def __len__(self) -> int:
        return math.ceil(self._data.shape[0] / self._batch_size)

    def __iter__(self) -> Iterator[dict[str, jnp.ndarray]]:
        return (self[i] for i in range(len(self)))

    def shuffle(self, seed: int) -> "Data":
        rng = np.random.default_rng(seed)
        return Data(
            self._data,
            self._n_questions,
            self._batch_size,
            {
                i: j
                for i, j in enumerate(rng.integers(0, len(self), len(self)))
            },
        )

    @property
    def batch_size(self) -> int:
        return self._batch_size

    @property
    def n_questions(self) -> int:
        return self._n_questions


############
# external #
############


def parse_config(config: dict[str, Any] | BaseModel | None = None) -> Config:
    if config is None:
        config = {}
    elif isinstance(config, BaseModel):
        config = config.dict()
    module = get_module(config.get("type", default_dataset))
    try:
        return module.Config(**config)  # type: ignore
    except AttributeError as exc:
        raise NotImplementedError() from exc


def add_comments(config: Config) -> CommentedMap:
    module = get_module(config.type)
    try:
        config_ = module.add_comments(config)  # type: ignore
    except AttributeError as exc:
        raise NotImplementedError() from exc
    config_.yaml_add_eol_comment(
        f"type of data ({', '.join(DATASETS)})",
        "type",
    )
    config_.yaml_add_eol_comment(
        "field corresponding to the user id",
        "user_field",
    )
    config_.yaml_add_eol_comment(
        "field corresponding to the timestamp",
        "timestamp_field",
    )
    config_.yaml_add_eol_comment(
        "field corresponding to the question id",
        "question_field",
    )
    config_.yaml_add_eol_comment(
        "field corresponding to the answer correctness",
        "answer_field",
    )
    config_.yaml_add_eol_comment(
        "proportion of data to put in validation set (0 <= p <= 1)",
        "p_valid",
    )
    config_.yaml_add_eol_comment(
        "proportion of data to put in test set (0 <= p <= 1)",
        "p_test",
    )
    config_.yaml_add_eol_comment(
        "random seed to use (set to 0 to have new seed)", "seed"
    )
    config_.yaml_add_eol_comment(
        "hash of the config (automatically modified)", "hash"
    )
    return config_


def read_data(
    config: Config, data_dir: Path, cache_dir: Path, echo: bool = True
) -> pl.DataFrame:
    cache_path = cache_dir / "data" / f"{config.type}:{config.hash}.ipc"

    if cache_path.exists():
        load_print("Reading cached data...", echo=echo)
        data = pl.read_ipc(cache_path)

    else:
        data = _read_data(config, data_dir, cache_dir, echo=echo)
        data = _split_data(data, config, echo=echo)
        load_print("Caching data...", echo=echo)
        cache_path.parent.mkdir(parents=True, exist_ok=True)
        data.write_ipc(cache_path)

    done_print("Read data.", echo=echo)

    return data


def convert_data(
    data: pl.DataFrame,
    config: Config,
    batch_size: int,
    cache_dir: Path,
    question2index: dict[int, int] | None = None,
    echo: bool = True,
) -> tuple[Data, dict[int, int]]:
    if question2index is None:
        question2index = _create_question2index(data, config.question_field)

    if "dataset" in data.columns:
        cache_path = (
            cache_dir
            / "data"
            / f"{config.type}:{data[0, 'dataset']}:sequences:{config.hash}.ipc"
        )
    else:
        cache_path = None

    if cache_path is not None and cache_path.exists():
        load_print("Reading cached sequence data...", echo=echo)
        data = pl.read_ipc(cache_path)

    else:
        load_print("Converting questions to indices...", echo=echo)
        data = data.with_column(
            pl.col(config.question_field).apply(question2index.get)
        )
        load_print("Converting data to sequences...", echo=echo)
        data = _convert_to_sequences(
            data, config.user_field, config.timestamp_field
        )
        data = data.rename(
            {
                config.user_field: "user",
                config.question_field: "questions",
                config.answer_field: "answers",
            }
        )
        if cache_path is not None:
            load_print("Saving cached sequence data...", echo=echo)
            cache_path.parent.mkdir(parents=True, exist_ok=True)
            data.write_ipc(cache_path)

    # the + 1 is added to account for the padding value 0
    return Data(data, len(question2index) + 1, batch_size), question2index


############
# internal #
############


def _read_data(
    config: Config, data_dir: Path, cache_dir: Path, echo: bool = True
) -> pl.DataFrame:
    module = get_module(config.type)
    try:
        return module.read_data(  # type: ignore
            config, data_dir, cache_dir, echo=echo
        )
    except AttributeError as exc:
        raise NotImplementedError() from exc


def _split_data(
    data: pl.DataFrame, config: Config, echo: bool = True
) -> pl.DataFrame:
    load_print("Extracting users...", echo=echo)
    users = data[config.user_field].unique().to_frame()

    load_print("Shuffling users...", echo=echo)
    users = users.sample(frac=1, shuffle=True, seed=config.seed)

    load_print("Splitting data...", echo=echo)
    n_valid = int(users.shape[0] * config.p_valid)
    n_test = int(users.shape[0] * config.p_test)
    n_train = users.shape[0] - n_valid - n_test
    users = users.with_column(
        pl.Series(
            "dataset",
            ["train"] * n_train + ["valid"] * n_valid + ["test"] * n_test,
        )
    )
    data = data.join(users, on=config.user_field)

    return data


def _create_question2index(
    data: pl.DataFrame, question_field: str
) -> dict[int, int]:
    return {
        q: i + 1  # type: ignore
        for i, q in enumerate(data[question_field].unique().sort().to_list())
    }


def _convert_to_sequences(
    data: pl.DataFrame, by: str, timestamp: str
) -> pl.DataFrame:
    if "dataset" in data.columns:
        by_ = ["dataset", by]
    else:
        by_ = [by]
    return (
        data.sort(timestamp)
        .groupby(by_)
        .agg(
            [
                *[
                    pl.list(feature)
                    for feature in data.columns
                    if feature not in by_
                ],
                pl.count(timestamp).alias("seq_len"),
            ]
        )
        .sort(["seq_len", *by_])
    )


def _create_batch(data: pl.DataFrame) -> dict[str, jnp.ndarray]:
    seq_len = int(data["seq_len"].max())  # type: ignore
    batch = {
        "user": jnp.array(data["user"].to_list()),
        "questions": jnp.array(
            [[0] * (seq_len - len(d)) + list(d) for d in data["questions"]]
        ),
        "answers": jnp.array(
            [[0] * (seq_len - len(d)) + list(d) for d in data["answers"]]
        ),
    }
    return {
        **batch,
        "prev_answers": jnp.concatenate(
            [
                jnp.zeros_like(batch["answers"][:, :1]),
                batch["answers"][:, :-1],
            ],
            axis=1,
        ),
    }
