from pathlib import Path

import httpx
import polars as pl
from ruamel.yaml.comments import CommentedMap

from kt.utils.print import load_print
from kt.utils.validators import set_field

from ..data import Config as BaseConfig

#########
# types #
#########


class Config(BaseConfig):
    _set_type = set_field("type", "synthetic_5")


############
# external #
############


def add_comments(config: Config) -> CommentedMap:
    return CommentedMap(config.dict())


def read_data(
    config: Config, data_dir: Path, cache_dir: Path, echo: bool = True
) -> pl.DataFrame:
    raw_path = data_dir / "synthetic_5" / "data.csv"
    cache_path = cache_dir / f"{config.hash}.ipc"

    if cache_path.exists():
        load_print("Reading cached synthetic 5 data...", echo=echo)
        data = pl.read_ipc(cache_path)
    elif raw_path.exists():
        load_print("Reading raw synthetic 5 data...", echo=echo)
        data = pl.read_csv(raw_path)
        load_print("Saving cached synthetic 5 data...", echo=echo)
        if not cache_path.parent.exists():
            cache_path.parent.mkdir(parents=True)
        data.write_ipc(cache_path)
    else:
        data_ = download_data(echo=echo)
        data = convert_data(data_, echo=echo)
        if not raw_path.parent.exists():
            raw_path.parent.mkdir(parents=True)
        load_print("Saving raw synthetic 5 data...", echo=echo)
        data.write_csv(raw_path)
        if not cache_path.parent.exists():
            cache_path.parent.mkdir(parents=True)
        load_print("Saving cached synthetic 5 data...", echo=echo)
        data.write_ipc(cache_path)

    return data


############
# internal #
############


def download_data(echo: bool = True) -> list[list[int]]:
    url = (
        "https://raw.githubusercontent.com/chrispiech/DeepKnowledgeTracing/"
        "master/data/synthetic/naive_c2_q50_s4000_v19.csv"
    )

    path = Path("/tmp") / "synthetic_5.csv"  # nosec

    load_print("Connecting to server...", echo=echo)
    with httpx.stream("GET", url) as resp:
        resp.raise_for_status()
        with open(path, "wb") as fb:
            size = 0
            for chunk in resp.iter_bytes():
                if chunk:
                    fb.write(chunk)
                    size += len(chunk)
                    if size < 1024**2:
                        load_print(
                            "Downloading data...",
                            f"{size / 1024:.2f} KB",
                            echo=echo,
                        )
                    elif size < 1024**3:  # pragma: no cover
                        load_print(
                            "Downloading data...",
                            f"{size / 1024**2:.2f} MB",
                            echo=echo,
                        )
                    else:  # pragma: no cover
                        load_print(
                            "Downloading data...",
                            f"{size / 1024**3:.2f} GB",
                            echo=echo,
                        )

    load_print("Reading downloaded data...", echo=echo)

    with open(path, "r") as f:
        data = [
            [int(answer) for answer in line.strip().split(",")] for line in f
        ]

    return data


def convert_data(data: list[list[int]], echo: bool = True) -> pl.DataFrame:
    load_print("Converting synthetic 5 data...", echo=echo)
    return pl.DataFrame(
        [
            {
                "student": i,
                "question": j,
                "answer": correct,
                "timestamp": j,
            }
            for i, line in enumerate(data)
            for j, correct in enumerate(line)
        ]
    )
