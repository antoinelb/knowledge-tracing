from typing import Any

import jax.numpy as jnp
import polars as pl
from pydantic import BaseModel


def equals(a: Any, b: Any, list_equiv_tuple: bool = False) -> bool:
    if type(a) != type(b) and (
        not list_equiv_tuple
        or isinstance(a, (tuple, list)) != isinstance(b, (tuple, list))
    ):
        return False

    if isinstance(a, (tuple, list)):
        if len(a) != len(b):
            return False
        else:
            return all(
                equals(aa, bb, list_equiv_tuple) for aa, bb in zip(a, b)
            )

    elif isinstance(a, pl.DataFrame):
        a = pl.DataFrame([a[field] for field in sorted(a.columns)])
        b = pl.DataFrame([b[field] for field in sorted(b.columns)])
        return a.frame_equal(b)

    elif isinstance(a, dict):
        try:
            return all(equals(a[k], b[k], list_equiv_tuple) for k in a.keys())
        except KeyError:
            return False

    elif isinstance(a, BaseModel):
        a_ = {k: v for k, v in a.dict().items() if k not in ("seed", "hash")}
        b_ = {k: v for k, v in b.dict().items() if k not in ("seed", "hash")}
        return equals(a_, b_, list_equiv_tuple)

    elif isinstance(a, jnp.ndarray):
        return jnp.array_equal(a, b)

    else:
        return a == b


def is_correct_type(a: Any, type_: Any) -> bool:
    if isinstance(a, (tuple, list)):
        if len(a) != len(type_):
            return False
        else:
            return all(is_correct_type(aa, t) for aa, t in zip(a, type_))

    elif isinstance(a, dict):
        try:
            return all(is_correct_type(a[k], type_[k]) for k in a.keys())
        except KeyError:
            return False

    elif type_ is None:
        return a is None

    else:
        return isinstance(a, type_)


def remove_key(dict_: dict[str, Any], key: str) -> dict[str, Any]:
    return {k: v for k, v in dict_.items() if k != key}
