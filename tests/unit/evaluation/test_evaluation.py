import io
import math
from functools import partial
from pathlib import Path

import jax
import jax.numpy as jnp
import pytest
from _pytest.capture import CaptureFixture
from pytest_mock import MockerFixture
from ruamel.yaml.comments import CommentedMap

from kt.data import Data
from kt.evaluation.evaluation import (
    Config,
    _evaluate,
    _read_data,
    _save_results,
    add_comments,
    evaluate,
    read_results,
)
from kt.model import ModelFct
from kt.utils.metrics import get_metric_fct
from kt.utils.print import load_progress
from tests.utils import equals

#########
# types #
#########


def test_splits_ignored_in_hash() -> None:
    config = Config(splits=["train", "valid"], seed=123)
    config_ = Config(splits=["train"], seed=123)
    assert config.hash == config_.hash


############
# external #
############


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 5
    assert len(config_) == 6
    assert "data" in config_
    assert "splits" in config_.ca.items
    assert "metrics" in config_.ca.items
    assert "batch_size" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items


@pytest.mark.parametrize("echo", [True, False])
def test_evaluate(
    capsys: CaptureFixture,
    mocker: MockerFixture,
    data_dir: Path,
    results_dir: Path,
    cache_dir: Path,
    model_fct: ModelFct,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    echo: bool,
) -> None:
    config = Config(
        data={"type": "synthetic_5", "p_valid": 0.2, "p_test": 0.1},
        metrics=["accuracy"],
        batch_size=4,
        splits=["train", "valid"],
    )
    _, _, _, question2index = tiny_data

    results_path = results_dir / "test_model"
    with io.StringIO() as output_:
        mocker.patch(
            "kt.evaluation.evaluation.load_progress",
            partial(load_progress, file=output_),
        )
        results_0 = evaluate(
            model_fct,
            question2index,
            config,
            data_dir,
            results_path,
            cache_dir,
            echo=echo,
        )
        output = output_.getvalue()

    assert "train" in results_0
    assert "accuracy" in results_0["train"]
    assert "valid" in results_0
    assert "accuracy" in results_0["valid"]
    assert "test" not in results_0

    if echo:
        assert "Evaluating train" in output
        assert "Evaluating valid" in output
        output = capsys.readouterr().out
        assert "Evaluation done." in output
    else:
        assert not output
        assert not capsys.readouterr().out

    config.splits = ["train", "valid", "test"]

    with io.StringIO() as output_:
        mocker.patch(
            "kt.evaluation.evaluation.load_progress",
            partial(load_progress, file=output_),
        )
        results_1 = evaluate(
            model_fct,
            question2index,
            config,
            data_dir,
            results_path,
            cache_dir,
            echo=echo,
        )
        output = output_.getvalue()

    assert "train" in results_1
    assert "accuracy" in results_1["train"]
    assert equals(results_1["train"], results_0["train"])
    assert "valid" in results_1
    assert "accuracy" in results_1["valid"]
    assert equals(results_1["valid"], results_0["valid"])
    assert "test" in results_1
    assert "accuracy" in results_1["test"]

    if echo:
        assert "Evaluating train" not in output
        assert "Evaluating valid" not in output
        assert "Evaluating test" in output
        output = capsys.readouterr().out
        assert "Evaluation done." in output
    else:
        assert not output
        assert not capsys.readouterr().out

    def other_model_fct(
        key: jax.random.KeyArray, inputs: dict[str, jnp.ndarray]
    ) -> dict[str, jnp.ndarray]:
        return {k: v / 2 for k, v in model_fct(key, inputs).items()}

    with io.StringIO() as output_:
        mocker.patch(
            "kt.evaluation.evaluation.load_progress",
            partial(load_progress, file=output_),
        )
        results_2 = evaluate(
            other_model_fct,
            question2index,
            config,
            data_dir,
            results_path,
            cache_dir,
            echo=echo,
        )
        output = output_.getvalue()

    assert equals(results_2, results_1)

    if echo:
        assert "Evaluating train" not in output
        assert "Evaluating valid" not in output
        assert "Evaluating test" not in output
        output = capsys.readouterr().out
        assert "Evaluation done." in output
    else:
        assert not output
        assert not capsys.readouterr().out

    with io.StringIO() as output_:
        mocker.patch(
            "kt.evaluation.evaluation.load_progress",
            partial(load_progress, file=output_),
        )
        results_3 = evaluate(
            other_model_fct,
            question2index,
            config,
            data_dir,
            results_path,
            cache_dir,
            force_eval=True,
            echo=echo,
        )
        output = output_.getvalue()

    assert not equals(results_3, results_1)

    if echo:
        assert "Evaluating train" in output
        assert "Evaluating valid" in output
        assert "Evaluating test" in output
        output = capsys.readouterr().out
        assert "Evaluation done." in output
    else:
        assert not output
        assert not capsys.readouterr().out


############
# internal #
############


@pytest.mark.parametrize("echo", [True, False])
def test_read_data(
    capsys: CaptureFixture,
    data_dir: Path,
    cache_dir: Path,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    echo: bool,
) -> None:
    config = Config(
        data={"type": "synthetic_5", "p_valid": 0.2, "p_test": 0.1}
    )
    _, _, _, question2index = tiny_data

    data = _read_data(
        config.data,
        ["train"],
        question2index,
        16,
        data_dir,
        cache_dir,
        echo=echo,
    )
    assert len(data) == 1
    assert len(data["train"]) == math.ceil(100 * 0.7 / 16)

    data = _read_data(
        config.data,
        ["train", "valid", "test", "test"],
        question2index,
        16,
        data_dir,
        cache_dir,
        echo=echo,
    )
    assert len(data) == 3
    assert len(data["train"]) == math.ceil(100 * 0.7 / 16)
    assert len(data["valid"]) == math.ceil(100 * 0.2 / 16)
    assert len(data["test"]) == math.ceil(100 * 0.1 / 16)


@pytest.mark.parametrize("echo", [True, False])
def test_evaluate_(
    mocker: MockerFixture,
    model_fct: ModelFct,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    echo: bool,
) -> None:
    metrics = [("accuracy", get_metric_fct("accuracy"))]
    _, _, data, _ = tiny_data

    def bad_model(
        key: jax.random.KeyArray, inputs: dict[str, jnp.ndarray]
    ) -> dict[str, jnp.ndarray]:
        return {
            "predictions": jax.random.uniform(key, inputs["questions"].shape)
        }

    with io.StringIO() as output_:
        mocker.patch(
            "kt.evaluation.evaluation.load_progress",
            partial(load_progress, file=output_),
        )
        good_results, good_predictions = _evaluate(
            model_fct,
            jax.random.PRNGKey(123),
            "test",
            data,
            metrics,  # type: ignore
            echo=echo,
        )
        output = output_.getvalue()

    assert "accuracy" in good_results
    assert set(good_predictions.columns) == {
        "predictions",
        "questions",
        "prev_answers",
        "answers",
        "user",
    }

    if echo:
        assert "Evaluating test:" in output
    else:
        assert not output

    bad_results, _ = _evaluate(
        bad_model,
        jax.random.PRNGKey(123),
        "test",
        data,
        metrics,  # type: ignore
        echo=echo,
    )
    assert bad_results["accuracy"] < good_results["accuracy"]

    with io.StringIO() as output_:
        mocker.patch(
            "kt.evaluation.evaluation.load_progress",
            partial(load_progress, file=output_),
        )
        _evaluate(
            model_fct,
            jax.random.PRNGKey(123),
            "valid",
            data,
            metrics,  # type: ignore
            echo=echo,
        )
        output = output_.getvalue()

    if echo:
        assert "Evaluating valid:" in output
    else:
        assert not output


def test_read_save_results(
    model_fct: ModelFct,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    results_dir: Path,
) -> None:
    metrics = [("accuracy", get_metric_fct("accuracy"))]
    _, valid_data, test_data, _ = tiny_data
    path = results_dir / "test_model"
    valid_results, valid_predictions = _evaluate(
        model_fct,
        jax.random.PRNGKey(123),
        "valid",
        valid_data,
        metrics,  # type: ignore
        echo=False,
    )
    test_results, test_predictions = _evaluate(
        model_fct,
        jax.random.PRNGKey(123),
        "test",
        test_data,
        metrics,  # type: ignore
        echo=False,
    )
    results = {"valid": valid_results, "test": test_results}
    predictions = {"valid": valid_predictions, "test": test_predictions}
    _save_results(results, predictions, path)  # type: ignore
    results_, predictions_ = read_results(path)
    assert equals(results, results_)
    assert equals(predictions, predictions_)
