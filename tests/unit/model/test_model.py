import math
import shutil
from pathlib import Path

import jax
import pytest
from _pytest.capture import CaptureFixture
from ruamel.yaml.comments import CommentedMap

from kt.data import Data
from kt.data.datasets import synthetic_5
from kt.data.utils import DATASETS
from kt.model.model import (
    Config,
    _create_model_fct,
    _read_data,
    _read_model_state,
    add_comments,
    get_model,
)
from kt.model.models import ModelFcts, dkt
from kt.model.models.utils import MODELS
from kt.utils.jax import read_tree, save_tree
from tests.utils import equals, remove_key

#########
# types #
#########


@pytest.mark.parametrize("type_", DATASETS)
def test_config_data_parsing(type_: str) -> None:
    config = Config(data={"type": type_})
    if type_ == "synthetic_5":
        assert isinstance(config.data, synthetic_5.Config)
    else:
        assert 0


@pytest.mark.parametrize("type_", MODELS)
def test_config_model_parsing(type_: str) -> None:
    config = Config(model={"type": type_})
    if type_ == "dkt":
        assert isinstance(config.model, dkt.Config)
    else:
        assert 0


############
# external #
############


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 1
    assert len(config_) == 4
    assert "data" in config_
    assert "model" in config_
    assert "train" in config_
    assert "hash" in config_.ca.items


@pytest.mark.parametrize("echo", [True, False])
def test_get_model(
    capsys: CaptureFixture,
    config: Config,
    data_dir: Path,
    model_dir: Path,
    cache_dir: Path,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    echo: bool,
) -> None:
    config = config.model  # type: ignore
    config.train.max_epoch = 1
    _, _, data, _ = tiny_data
    key = jax.random.PRNGKey(0)
    model_path = model_dir / config.data.type / config.model.type / config.hash

    model, question2index_0, results_0 = get_model(
        config, data_dir, model_dir, cache_dir, echo=echo
    )
    model = jax.vmap(model, [None, 0], 0)
    predictions_0 = [remove_key(model(key, inputs), "time") for inputs in data]
    if echo:
        output = capsys.readouterr().out
        assert "Reading previous training state..." not in output
        assert "Trained model." in output
    else:
        assert not capsys.readouterr().out

    model, question2index_1, results_1 = get_model(
        config, data_dir, model_dir, cache_dir, echo=echo
    )
    model = jax.vmap(model, [None, 0], 0)
    predictions_1 = [remove_key(model(key, inputs), "time") for inputs in data]
    assert equals((predictions_0), predictions_1)
    assert equals(question2index_0, question2index_1)
    assert equals(results_0, results_1)
    if echo:
        output = capsys.readouterr().out
        assert "Reading previous training state..." in output
        assert "Read trained model." in output
    else:
        assert not capsys.readouterr().out

    save_tree(
        jax.tree_util.tree_map(
            lambda x: x + 0.1, read_tree(model_path / "0" / "params")
        ),
        model_path / "0" / "params",
    )
    model, question2index_2, results_2 = get_model(
        config, data_dir, model_dir, cache_dir, echo=echo
    )
    model = jax.vmap(model, [None, 0], 0)
    predictions_2 = [remove_key(model(key, inputs), "time") for inputs in data]
    assert not equals(predictions_0, predictions_2)
    assert equals(question2index_0, question2index_2)
    assert equals(results_0, results_2)
    if echo:
        output = capsys.readouterr().out
        assert "Reading previous training state..." in output
        assert "Read trained model." in output
    else:
        assert not capsys.readouterr().out

    model, question2index_3, results_3 = get_model(
        config,
        data_dir,
        model_dir,
        cache_dir,
        force_training=True,
        echo=echo,
    )
    model = jax.vmap(model, [None, 0], 0)
    predictions_3 = [remove_key(model(key, inputs), "time") for inputs in data]
    assert equals(predictions_0, predictions_3)
    assert equals(question2index_0, question2index_3)
    assert equals(results_0, results_3)
    assert not equals(predictions_2, predictions_3)
    assert equals(question2index_2, question2index_3)
    assert equals(results_2, results_3)
    if echo:
        output = capsys.readouterr().out
        assert "Reading previous training state..." not in output
        assert "Trained model." in output
    else:
        assert not capsys.readouterr().out


############
# internal #
############


def test_create_model_fct(tiny_dkt: ModelFcts, fake_data: Data) -> None:
    init, predict, _ = tiny_dkt
    params = init(jax.random.PRNGKey(0), fake_data.n_questions)
    observation = {key: vals[0] for key, vals in fake_data[0].items()}

    model_fct = _create_model_fct(predict, params)
    predictions = model_fct(jax.random.PRNGKey(1), observation)
    assert "time" in predictions

    assert equals(
        remove_key(predictions, "time"),
        predict(
            params,
            jax.random.PRNGKey(1),
            observation,
            False,
        ),
    )


@pytest.mark.parametrize("echo", [True, False])
def test_read_data(
    capsys: CaptureFixture, data_dir: Path, cache_dir: Path, echo: bool
) -> None:
    config = Config(
        data={"type": "synthetic_5", "p_valid": 0.2, "p_test": 0.1}
    )

    train_data, valid_data, question2index = _read_data(
        config.data, 16, data_dir, cache_dir, echo=echo
    )

    assert len(train_data) == math.ceil(100 * 0.7 / 16)
    assert len(valid_data) == math.ceil(100 * 0.2 / 16)
    assert len(question2index) == 50


@pytest.mark.parametrize("echo", [True, False])
def test_read_model_state(
    capsys: CaptureFixture,
    config: Config,
    data_dir: Path,
    model_dir: Path,
    cache_dir: Path,
    echo: bool,
) -> None:
    config = config.model  # type: ignore
    config.train.max_epoch = 2
    model_path = model_dir / config.data.type / config.model.type / config.hash

    model_state = _read_model_state(model_path, echo=echo)
    assert model_state is None

    _, question2index, _ = get_model(
        config, data_dir, model_dir, cache_dir, echo=echo
    )

    model_state = _read_model_state(model_path, echo=echo)
    assert equals(question2index, model_state[0])  # type: ignore
    assert len(model_state[1].train_results) == 2  # type: ignore
    assert len(model_state[1].valid_results) == 2  # type: ignore

    shutil.rmtree(model_path / "0")
    shutil.rmtree(model_path / "1")
    model_state = _read_model_state(model_path, echo=echo)
    assert model_state is None

    if echo:
        assert capsys.readouterr().out
    else:
        assert not capsys.readouterr().out
