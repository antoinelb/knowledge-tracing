from functools import partial

import jax
import jax.numpy as jnp
from ruamel.yaml import CommentedMap

from kt.model.models.base import InitFct, LossFct, PredictFct
from kt.model.models.dkt import (
    Config,
    _create_model,
    add_comments,
    create_model,
)
from tests.utils import equals

#########
# types #
#########


def test_type() -> None:
    assert Config().type == "dkt"


############
# external #
############


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 1
    assert "n_hid" in config_.ca.items


def test_create_model(
    model: tuple[InitFct, PredictFct, LossFct] | None = None
) -> None:
    config = Config(n_hid=[2, 3, 4])
    if model is None:
        init, predict, loss = create_model(config)
    else:
        init, predict, loss = model

    # test correct shape of initialized params
    params = init(jax.random.PRNGKey(0), 5)
    assert len(params) == 2
    assert [[pp.shape for pp in p] for p in params[0]] == [
        [(10, 2), (2, 2), (2,)],
        [(2, 3), (3, 3), (3,)],
        [(3, 4), (4, 4), (4,)],
    ]
    assert [p.shape for p in params[1]] == [(4, 5), (5,)]  # type: ignore

    # test correct shape of predictions
    predictions = predict(
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([1, 2, 3]),
            "prev_answers": jnp.array([0, 1, 0]),
        },
        True,
    )
    assert len(predictions) == 1
    assert predictions["predictions"].shape == (3,)  # type: ignore
    # test no difference in prediction values between training and inference
    assert equals(
        predict(
            params,
            jax.random.PRNGKey(0),
            {
                "questions": jnp.array([1, 2, 3]),
                "prev_answers": jnp.array([0, 1, 0]),
            },
            False,
        ),
        predictions,
    )
    # test no difference in prediction values between training and inference
    # when using jit
    assert equals(
        jax.jit(predict)(
            params,
            jax.random.PRNGKey(0),
            {
                "questions": jnp.array([1, 2, 3]),
                "prev_answers": jnp.array([0, 1, 0]),
            },
            True,
        ),
        predictions,
    )
    # test correct shape of batched predictions
    v_predict = jax.vmap(predict, (None, None, 0, None), 0)
    predictions = v_predict(
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([[1, 2, 3], [3, 2, 1]]),
            "prev_answers": jnp.array([[0, 1, 0], [1, 0, 1]]),
        },
        True,
    )
    assert len(predictions) == 1
    assert predictions["predictions"].shape == (2, 3)  # type: ignore

    # test loss is a valid number
    loss_ = loss(
        predict,
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([1, 2, 3]),
            "prev_answers": jnp.array([0, 1, 0]),
            "answers": jnp.array([1, 0, 1]),
        },
        True,
    )
    assert not jnp.isnan(loss_)
    assert loss_.shape == ()
    # test batched loss is a valid number
    v_loss_ = loss(
        v_predict,
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([[1, 2, 3], [3, 2, 1]]),
            "prev_answers": jnp.array([[0, 1, 0], [1, 0, 1]]),
            "answers": jnp.array([[1, 0, 1], [0, 1, 0]]),
        },
        True,
    )
    assert not jnp.isnan(v_loss_)
    assert v_loss_.shape == ()
    # test jit loss is the same
    assert loss_ == jax.jit(partial(loss, predict))(
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([1, 2, 3]),
            "prev_answers": jnp.array([0, 1, 0]),
            "answers": jnp.array([1, 0, 1]),
        },
        True,
    )
    # test jit batched loss is the same
    assert v_loss_ == jax.jit(partial(loss, v_predict))(
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([[1, 2, 3], [3, 2, 1]]),
            "prev_answers": jnp.array([[0, 1, 0], [1, 0, 1]]),
            "answers": jnp.array([[1, 0, 1], [0, 1, 0]]),
        },
        True,
    )


############
# internal #
############


def test_create_model_() -> None:
    config = Config(n_hid=[2, 3, 4])
    init, fct = _create_model(config)

    params = init(jax.random.PRNGKey(0), 5)
    assert len(params) == 2
    assert [[pp.shape for pp in p] for p in params[0]] == [
        [(10, 2), (2, 2), (2,)],
        [(2, 3), (3, 3), (3,)],
        [(3, 4), (4, 4), (4,)],
    ]
    assert [p.shape for p in params[1]] == [(4, 5), (5,)]  # type: ignore

    predictions = fct(
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([1, 2, 3]),
            "prev_answers": jnp.array([0, 1, 0]),
        },
        True,
    )
    assert len(predictions) == 1
    assert predictions["predictions"].shape == (3,)  # type: ignore

    assert equals(
        fct(
            params,
            jax.random.PRNGKey(0),
            {
                "questions": jnp.array([1, 2, 3]),
                "prev_answers": jnp.array([0, 1, 0]),
            },
            False,
        ),
        predictions,
    )

    v_fct = jax.vmap(fct, (None, None, 0, None), 0)
    predictions = v_fct(
        params,
        jax.random.PRNGKey(0),
        {
            "questions": jnp.array([[1, 2, 3], [4, 5, 6]]),
            "prev_answers": jnp.array([[0, 1, 0], [1, 0, 1]]),
        },
        True,
    )
    assert len(predictions) == 1
    assert predictions["predictions"].shape == (2, 3)  # type: ignore
