from unittest.mock import Mock

import pytest
from pytest_mock import MockerFixture
from ruamel.yaml import CommentedMap

from kt.model.models import dkt
from kt.model.models.base import (
    Config,
    add_comments,
    create_model,
    parse_config,
)
from kt.model.models.utils import MODELS
from tests.utils import equals

from .test_dkt import test_create_model as test_create_model_

############
# external #
############


@pytest.mark.parametrize("type_", MODELS)
def test_parse_config(type_: str) -> None:
    if type_ == "dkt":
        assert isinstance(
            parse_config({"type": type_}),
            dkt.Config,
        )
        assert isinstance(
            parse_config(dkt.Config()),
            dkt.Config,
        )
    else:
        assert 0


def test_parse_config__no_type_() -> None:
    assert equals(parse_config({}), dkt.Config())
    assert equals(parse_config(), dkt.Config())
    assert equals(parse_config(None), dkt.Config())


def test_parse_config__wrong(mocker: MockerFixture) -> None:
    module = Mock()
    module.Config.side_effect = AttributeError()
    mocker.patch("kt.model.models.base.get_module", return_value=module)
    with pytest.raises(NotImplementedError):
        parse_config({"type": "wrong"})


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) >= 3
    assert len(config_) >= 3
    assert "type" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items


def test_add_comments__wrong(mocker: MockerFixture) -> None:
    module = Mock()
    module.add_comments.side_effect = AttributeError()
    mocker.patch("kt.model.models.base.get_module", return_value=module)
    config = Config()
    with pytest.raises(NotImplementedError):
        add_comments(config)


@pytest.mark.parametrize("type_", MODELS)
def test_create_model(type_: str) -> None:
    if type_ == "dkt":
        config = parse_config({"type": type_, "n_hid": [2, 3, 4]})
        test_create_model_(create_model(config))
    else:
        assert 0


def test_create_model__wrong(mocker: MockerFixture) -> None:
    module = Mock()
    module.create_model.side_effect = AttributeError()
    mocker.patch("kt.model.models.base.get_module", return_value=module)
    config = Config()
    with pytest.raises(NotImplementedError):
        create_model(config)
