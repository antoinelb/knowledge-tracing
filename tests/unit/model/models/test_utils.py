import pytest

from kt.model.models.utils import MODELS, get_module


def test_models() -> None:
    assert MODELS


@pytest.mark.parametrize("type_", MODELS)
def test_get_module(type_: str) -> None:
    assert f"kt.model.models.{type_}" in str(get_module(type_))


def test_get_module__wrong() -> None:
    with pytest.raises(NotImplementedError):
        get_module("wrong")
