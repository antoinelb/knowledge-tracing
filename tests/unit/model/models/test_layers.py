import jax
import jax.numpy as jnp

from kt.model.models.layers import _rnn_step, create_dense, create_rnn

############
# external #
############


def test_create_dense() -> None:
    init, fct = create_dense()
    params = init(jax.random.PRNGKey(123), 10, 20)
    assert len(params) == 2
    assert params[0].shape == (10, 20)
    assert params[1].shape == (20,)

    assert fct(params, jnp.arange(10)).shape == (20,)

    v_fct = jax.vmap(fct, (None, 0), 0)
    assert v_fct(params, jnp.arange(10 * 5).reshape(5, 10)).shape == (5, 20)


def test_create_rnn() -> None:
    init, fct = create_rnn()
    params = init(jax.random.PRNGKey(123), 10, 20)
    assert len(params) == 3
    assert params[0].shape == (10, 20)
    assert params[1].shape == (20, 20)
    assert params[2].shape == (20,)

    assert fct(
        params, jax.random.PRNGKey(0), jnp.arange(3 * 10).reshape(3, 10)
    ).shape == (3, 20)

    v_fct = jax.vmap(fct, (None, None, 0), 0)
    assert v_fct(
        params, jax.random.PRNGKey(0), jnp.arange(5 * 3 * 10).reshape(5, 3, 10)
    ).shape == (
        5,
        3,
        20,
    )


############
# internal #
############


def test_rnn_step() -> None:
    params = (
        jnp.arange(2 * 3).reshape(2, 3),
        jnp.arange(3 * 3).reshape(3, 3),
        jnp.arange(3),
    )
    inputs = jnp.array([1, 2])
    state = jnp.array([1, 2, 3])

    correct = (
        jnp.array([1 + 4, 3 + 8, 5 + 12])
        + jnp.array([1 + 4 + 9, 4 + 10 + 18, 7 + 16 + 27])
        + jnp.array([1, 2, 3])
    )
    result, result_ = _rnn_step(params, jax.nn.sigmoid, state, inputs)
    assert jnp.array_equal(result, jax.nn.sigmoid(correct))
    assert jnp.array_equal(result_, jax.nn.sigmoid(correct))
    assert result.shape == (3,)
    assert result_.shape == (3,)
    assert jnp.array_equal(result, result_)
