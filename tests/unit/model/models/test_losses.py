import jax
import jax.numpy as jnp
import numpy as np

from kt.model.models import Params
from kt.model.models.losses import compute_binary_cross_entropy


def test_compute_binary_cross_entropy() -> None:
    inputs = {
        "questions": jnp.array([1, 2, 3, 4]),
        "answers": jnp.array([1, 0, 1, 0]),
    }
    loss = compute_binary_cross_entropy(
        predict, [], jax.random.PRNGKey(0), inputs, True  # type: ignore
    )
    assert loss == -np.log(0.1) - np.log(0.8) - np.log(0.3) - np.log(0.6)

    inputs = {
        "questions": jnp.array([[1, 2, 3, 4], [5, 6, 7, 8]]),
        "answers": jnp.array([[1, 0, 1, 0], [0, 1, 0, 1]]),
    }
    loss = compute_binary_cross_entropy(
        predict, [], jax.random.PRNGKey(0), inputs, True  # type: ignore
    )
    assert (
        loss
        == (
            (-np.log(0.1) - np.log(0.8) - np.log(0.3) - np.log(0.6))
            + (-np.log(0.5) - np.log(0.6) - np.log(0.3) - np.log(0.8))
        )
        / 2
    )

    inputs = {
        "questions": jnp.array([[1, 2, 3, 4], [0, 0, 7, 8]]),
        "answers": jnp.array([[1, 0, 1, 0], [0, 0, 0, 1]]),
    }
    loss = compute_binary_cross_entropy(
        predict, [], jax.random.PRNGKey(0), inputs, True  # type: ignore
    )
    assert (
        loss
        == (
            (-np.log(0.1) - np.log(0.8) - np.log(0.3) - np.log(0.6))
            + (-np.log(0.3) - np.log(0.8))
        )
        / 2
    )


#########
# utils #
#########


def predict(
    params: Params,
    key: jax.random.KeyArray,
    inputs: dict[str, jnp.ndarray],
    train: bool,
) -> dict[str, jnp.ndarray]:
    return {
        "predictions": (
            0.1 + jnp.arange(inputs["questions"].size) * 0.1
        ).reshape(*inputs["questions"].shape)
    }
