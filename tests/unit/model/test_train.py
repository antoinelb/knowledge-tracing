import io
import shutil
from functools import partial
from pathlib import Path

import jax
import optax
import pytest
from pytest_mock import MockerFixture
from ruamel.yaml import CommentedMap

from kt.data import Data
from kt.model.models import ModelFcts
from kt.model.train import (
    Config,
    TrainState,
    _create_run_batch_fct,
    _read_epoch,
    _read_epoch_results,
    _run_epoch,
    _save_epoch,
    add_comments,
    is_train_done,
    plot_training_curves,
    read_best,
    read_results,
    read_train_state,
    train,
)
from kt.utils.metrics import _accuracy
from kt.utils.print import load_progress
from tests.utils import equals

############
# external #
############


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 6
    assert len(config_) == 6
    assert "batch_size" in config_.ca.items
    assert "max_epoch" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "learning_rate" in config_.ca.items
    assert "metrics" in config_.ca.items
    assert "hash" in config_.ca.items


@pytest.mark.parametrize("echo", [True, False])
def test_train(
    mocker: MockerFixture,
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
    echo: bool,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=1, metrics=["accuracy"])
    model_path = model_dir / "test_model"

    v_predict = jax.vmap(predict, [None, None, 0, None], 0)

    params = init(jax.random.PRNGKey(0), train_data.n_questions)
    loss_0 = compute_loss(
        v_predict, params, jax.random.PRNGKey(1), valid_data[0], False
    )

    with io.StringIO() as output_:
        mocker.patch(
            "kt.model.train.load_progress",
            partial(load_progress, file=output_),
        )
        params, results = train(
            config,
            train_data,
            valid_data,
            init,
            predict,
            compute_loss,
            model_path,
            echo=echo,
        )
        output = output_.getvalue()

    loss_1 = compute_loss(
        v_predict, params, jax.random.PRNGKey(1), valid_data[0], False
    )

    assert loss_1 < loss_0
    assert len(results["train"]) == 1
    assert set(results["train"][0].keys()) == {"accuracy", "loss"}
    assert len(results["valid"]) == 1
    assert set(results["valid"][0].keys()) == {"accuracy", "loss"}

    if echo:
        assert "Training" in output
        assert "t_l:" in output
        assert "t_a:" in output
        assert "v_l:" in output
        assert "v_a:" in output
    else:
        assert not output


def test_train__deterministic(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=1, seed=123)
    model_path = model_dir / "test_model"

    params_0, results_0 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )
    shutil.rmtree(model_path)
    params_1, results_1 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )
    assert equals(params_0, params_1)
    assert equals(results_0, results_1)


def test_train__continues(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=1, seed=123)
    model_path = model_dir / "test_model"

    params_0, results_0 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )
    params_1, results_1 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )
    assert equals(params_0, params_1)
    assert equals(results_0, results_1)

    state = read_train_state(model_path)
    params_1, results_1 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        state,
        echo=False,
    )
    assert equals(params_0, params_1)
    assert equals(results_0, results_1)

    config = Config(batch_size=16, max_epoch=2, seed=123)
    params_2, results_2 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )
    assert not equals(params_0, params_2)
    assert not equals(results_0, results_2)
    shutil.rmtree(model_path / "1")
    state = read_train_state(model_path)
    params_3, results_3 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        state,
        echo=False,
    )
    assert equals(params_2, params_3)
    assert equals(results_2, results_3)
    state = TrainState(
        jax.tree_util.tree_map(lambda x: x + 0.01, state.params),  # type: ignore # noqa
        state.opt_state,  # type: ignore
        state.train_results,  # type: ignore
        state.valid_results,  # type: ignore
    )
    params_4, results_4 = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        state,
        echo=False,
    )
    assert not equals(params_0, params_4)
    assert not equals(results_0, results_4)
    assert not equals(params_2, params_4)
    assert not equals(results_2, results_4)


def test_is_train_done(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:

    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=2, seed=123)
    model_path = model_dir / "test_model"

    train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )

    assert not is_train_done(None, config)

    state = read_train_state(model_path)
    assert is_train_done(state, config)

    state = TrainState(
        state.params,  # type: ignore
        state.opt_state,  # type: ignore
        state.train_results[:1],  # type: ignore
        state.valid_results[:1],  # type: ignore
    )
    assert not is_train_done(state, config)


def test_read_state(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=2, seed=123)
    model_path = model_dir / "test_model"

    state = read_train_state(model_path)
    assert state is None

    params, results = train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )

    state = read_train_state(model_path)
    assert equals(state.params, params, list_equiv_tuple=True)  # type: ignore
    assert len(state.train_results) == 2  # type: ignore
    assert len(state.valid_results) == 2  # type: ignore
    assert equals(results["train"], state.train_results)  # type: ignore
    assert equals(results["valid"], state.valid_results)  # type: ignore


def test_read_save_best_epoch(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    predict = jax.vmap(predict, [None, None, 0, None], 0)
    optimizer = optax.adam(1e-3)
    path = model_dir / "3"

    update = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        train_data.batch_size,
        [("accuracy", _accuracy)],
        True,
    )
    eval_ = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        train_data.batch_size,
        [("accuracy", _accuracy)],
        False,
    )

    params = init(jax.random.PRNGKey(0), train_data.n_questions)
    opt_state = optimizer.init(params)

    params, opt_state, train_results = _run_epoch(
        update,
        params,
        jax.random.PRNGKey(1),
        train_data,
        opt_state,
        train=True,
        echo=False,
    )
    _, _, valid_results = _run_epoch(
        eval_,
        params,
        jax.random.PRNGKey(1),
        train_data,
        opt_state,
        train=True,
        echo=False,
    )

    assert not path.exists()

    _save_epoch(
        params,
        opt_state,
        train_results,
        valid_results,
        path,
        best=True,
    )

    assert path.exists()
    saved = read_best(path.parent)
    assert equals(params, saved, list_equiv_tuple=True)


def test_read_results(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=2, seed=123)
    model_path = model_dir / "test_model"

    state = read_train_state(model_path)
    assert state is None

    train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )

    results = read_results(model_path)
    assert len(results["train"]) == 2
    assert len(results["valid"]) == 2


def test_plot_training_curves(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    config = Config(batch_size=16, max_epoch=2, seed=123)
    model_path = model_dir / "test_model"

    train(
        config,
        train_data,
        valid_data,
        init,
        predict,
        compute_loss,
        model_path,
        echo=False,
    )

    results = read_results(model_path)
    plots = plot_training_curves(config, results)
    assert "loss" in plots
    for metric in config.metrics:
        assert metric in plots


############
# internal #
############


def test_create_run_batch_fct(tiny_dkt: ModelFcts, fake_data: Data) -> None:
    init, predict, compute_loss = tiny_dkt
    predict = jax.vmap(predict, [None, None, 0, None], 0)

    optimizer = optax.adam(1e-3)
    params_0 = init(jax.random.PRNGKey(0), fake_data.n_questions)
    opt_state_0 = optimizer.init(params_0)

    eval_ = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        fake_data.batch_size,
        [("accuracy", _accuracy)],
        False,
    )
    update = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        fake_data.batch_size,
        [("accuracy", _accuracy)],
        True,
    )

    # test eval doesn't change the parameters and opt state
    params_1, opt_state_1, results_1, predictions_1 = eval_(
        params_0, jax.random.PRNGKey(1), fake_data[0], opt_state_0
    )
    assert equals(params_0, params_1)
    assert equals(opt_state_0, opt_state_1)
    assert "accuracy" in results_1
    assert "loss" in results_1
    assert "predictions" in predictions_1

    # test eval doesn't change the predictions and results
    params_1, opt_state_1, results_2, predictions_2 = eval_(
        params_0, jax.random.PRNGKey(1), fake_data[0], opt_state_0
    )
    assert equals(results_1, results_2)
    assert equals(predictions_1, predictions_2)

    # test update changes the parameters and opt state
    params_1, opt_state_1, results_1, predictions_1 = update(
        params_0, jax.random.PRNGKey(1), fake_data[0], opt_state_0
    )
    assert not equals(params_0, params_1)
    assert not equals(opt_state_0, opt_state_1)
    assert "accuracy" in results_1
    assert "loss" in results_1
    assert "predictions" in predictions_1

    # test update ran multiple times gives better results
    params_2, opt_state_2, results_2, predictions_2 = update(
        params_1, jax.random.PRNGKey(1), fake_data[0], opt_state_1
    )
    assert not equals(params_1, params_2)
    assert not equals(opt_state_1, opt_state_2)
    assert not equals(predictions_1, predictions_2)
    assert results_2["accuracy"] > results_1["accuracy"]
    assert results_2["loss"] < results_1["loss"]


@pytest.mark.parametrize("echo", [True, False])
def test_run_epoch(
    mocker: MockerFixture, tiny_dkt: ModelFcts, fake_data: Data, echo: bool
) -> None:
    init, predict, compute_loss = tiny_dkt
    predict = jax.vmap(predict, [None, None, 0, None], 0)
    optimizer = optax.adam(1e-3)

    eval_ = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        fake_data.batch_size,
        [("accuracy", _accuracy)],
        False,
    )
    update = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        fake_data.batch_size,
        [("accuracy", _accuracy)],
        True,
    )

    params_0 = init(jax.random.PRNGKey(0), fake_data.n_questions)
    opt_state_0 = optimizer.init(params_0)

    # test eval doesn't change the parameters and opt state
    with io.StringIO() as output_:
        mocker.patch(
            "kt.model.train.load_progress",
            partial(load_progress, file=output_),
        )
        params_1, opt_state_1, results_1 = _run_epoch(
            eval_,
            params_0,
            jax.random.PRNGKey(1),
            fake_data,
            opt_state_0,
            train=False,
            echo=echo,
        )
        output = output_.getvalue()
        assert equals(params_0, params_1)
        assert equals(opt_state_0, opt_state_1)
        assert "accuracy" in results_1
        assert "loss" in results_1

        if echo:
            assert "Valid: " in output
            assert "Train: " not in output
            assert "l:" in output
            assert "a:" in output
        else:
            assert not output

    # test eval doesn't change the results and predictions
    with io.StringIO() as output_:
        mocker.patch(
            "kt.model.train.load_progress",
            partial(load_progress, file=output_),
        )
        params_1, opt_state_1, results_2 = _run_epoch(
            eval_,
            params_0,
            jax.random.PRNGKey(1),
            fake_data,
            opt_state_0,
            train=False,
            echo=echo,
        )
        output = output_.getvalue()
        assert equals(results_1, results_2)

        if echo:
            assert "Valid: " in output
            assert "Train: " not in output
            assert "l:" in output
            assert "a:" in output
        else:
            assert not output

    # test update changes the parameters and opt state
    with io.StringIO() as output_:
        mocker.patch(
            "kt.model.train.load_progress",
            partial(load_progress, file=output_),
        )
        params_1, opt_state_1, results_1 = _run_epoch(
            update,
            params_0,
            jax.random.PRNGKey(1),
            fake_data,
            opt_state_0,
            train=True,
            echo=echo,
        )
        output = output_.getvalue()
        assert not equals(params_0, params_1)
        assert not equals(opt_state_0, opt_state_1)
        assert "accuracy" in results_1
        assert "loss" in results_1

        if echo:
            assert "Valid: " not in output
            assert "Train: " in output
            assert "l:" in output
            assert "a:" in output
        else:
            assert not output

    # test update ran multiple times gives better results
    with io.StringIO() as output_:
        mocker.patch(
            "kt.model.train.load_progress",
            partial(load_progress, file=output_),
        )
        params_2, opt_state_2, results_2 = _run_epoch(
            update,
            params_1,
            jax.random.PRNGKey(1),
            fake_data,
            opt_state_1,
            train=True,
            echo=echo,
        )
        output = output_.getvalue()
        assert not equals(params_1, params_2)
        assert not equals(opt_state_1, opt_state_2)
        assert results_2["accuracy"] > results_1["accuracy"]
        assert results_2["loss"] < results_1["loss"]

        if echo:
            assert "Valid: " not in output
            assert "Train: " in output
            assert "l:" in output
            assert "a:" in output
        else:
            assert not output


def test_read_save_epoch(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    predict = jax.vmap(predict, [None, None, 0, None], 0)
    optimizer = optax.adam(1e-3)
    path = model_dir / "3"

    update = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        train_data.batch_size,
        [("accuracy", _accuracy)],
        True,
    )
    eval_ = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        train_data.batch_size,
        [("accuracy", _accuracy)],
        False,
    )

    params = init(jax.random.PRNGKey(0), train_data.n_questions)
    opt_state = optimizer.init(params)

    params, opt_state, train_results = _run_epoch(
        update,
        params,
        jax.random.PRNGKey(1),
        train_data,
        opt_state,
        train=True,
        echo=False,
    )
    _, _, valid_results = _run_epoch(
        eval_,
        params,
        jax.random.PRNGKey(1),
        train_data,
        opt_state,
        train=True,
        echo=False,
    )

    assert not path.exists()

    _save_epoch(
        params,
        opt_state,
        train_results,
        valid_results,
        path,
        best=False,
    )

    assert path.exists()
    saved = _read_epoch(path)
    assert equals(params, saved[0], list_equiv_tuple=True)
    assert equals(opt_state, saved[1])
    assert equals(train_results, saved[2])
    assert equals(valid_results, saved[3])


def test_read_epoch_results(
    tiny_dkt: ModelFcts,
    tiny_data: tuple[Data, Data, Data, dict[int, int]],
    model_dir: Path,
) -> None:
    init, predict, compute_loss = tiny_dkt
    train_data, valid_data, _, _ = tiny_data
    predict = jax.vmap(predict, [None, None, 0, None], 0)
    optimizer = optax.adam(1e-3)
    path = model_dir / "3"

    update = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        train_data.batch_size,
        [("accuracy", _accuracy)],
        True,
    )
    eval_ = _create_run_batch_fct(
        predict,
        compute_loss,
        optimizer,
        train_data.batch_size,
        [("accuracy", _accuracy)],
        False,
    )

    params = init(jax.random.PRNGKey(0), train_data.n_questions)
    opt_state = optimizer.init(params)

    params, opt_state, train_results = _run_epoch(
        update,
        params,
        jax.random.PRNGKey(1),
        train_data,
        opt_state,
        train=True,
        echo=False,
    )
    _, _, valid_results = _run_epoch(
        eval_,
        params,
        jax.random.PRNGKey(1),
        train_data,
        opt_state,
        train=True,
        echo=False,
    )

    assert not path.exists()

    _save_epoch(
        params,
        opt_state,
        train_results,
        valid_results,
        path,
        best=False,
    )

    results = _read_epoch_results(path)
    assert equals(results["train"], train_results)
    assert equals(results["valid"], valid_results)
