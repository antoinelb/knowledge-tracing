import re
import shutil
from pathlib import Path

import jax
import mlflow
import pytest
from _pytest.capture import CaptureFixture
from pytest_mock import MockerFixture
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap

from kt.evaluation import read_results
from kt.experiment import (
    Config,
    _add_comments,
    _flatten_dict,
    _init_mlflow,
    _log_mlflow_artifact,
    read_config,
    run_experiment,
    save_config,
)
from kt.utils.jax import read_tree, save_tree
from tests.utils import equals

############
# external #
############


@pytest.mark.parametrize("echo", [True, False])
def test_run_experiment(
    mocker: MockerFixture,
    capsys: CaptureFixture,
    config: Config,
    data_dir: Path,
    model_dir: Path,
    results_dir: Path,
    cache_dir: Path,
    mlflow_uri: str,
    echo: bool,
) -> None:
    mocker.patch("kt.experiment.mlflow")
    config.model.train.max_epoch = 1
    results_path = results_dir / f"{config.name}:{config.hash}"
    model_path = (
        model_dir
        / config.model.data.type
        / config.model.model.type
        / config.model.hash
    )
    assert not results_path.exists()

    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=echo,
    )
    assert results_path.exists()
    results_0, _ = read_results(results_path)
    shutil.rmtree(results_path)

    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=echo,
    )
    results_1, _ = read_results(results_path)
    assert equals(results_0, results_1)
    shutil.rmtree(results_path)

    save_tree(
        jax.tree_util.tree_map(
            lambda x: x + 0.1, read_tree(model_path / "0" / "params")
        ),
        model_path / "0" / "params",
    )
    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=echo,
    )
    results_2, _ = read_results(results_path)
    assert not equals(results_0, results_2)
    shutil.rmtree(results_path)

    save_tree(
        jax.tree_util.tree_map(
            lambda x: x + 0.1, read_tree(model_path / "0" / "params")
        ),
        model_path / "0" / "params",
    )
    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=True,
        echo=echo,
    )
    results_3, _ = read_results(results_path)
    assert equals(results_0, results_3)
    shutil.rmtree(results_path)

    config.model.train.max_epoch = 2

    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=echo,
    )
    results_4, _ = read_results(results_path)
    assert not equals(results_0, results_4)
    shutil.rmtree(results_path)

    shutil.rmtree(model_path / "1")
    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=echo,
    )
    results_5, _ = read_results(results_path)
    assert equals(results_4, results_5)
    shutil.rmtree(results_path)

    shutil.rmtree(model_path / "1")
    save_tree(
        jax.tree_util.tree_map(
            lambda x: x + 0.1, read_tree(model_path / "0" / "params")
        ),
        model_path / "0" / "params",
    )
    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=echo,
    )
    results_6, _ = read_results(results_path)
    assert not equals(results_0, results_6)
    assert not equals(results_4, results_6)
    shutil.rmtree(results_path)

    if echo:
        assert capsys.readouterr().out
    else:
        assert not capsys.readouterr().out

    assert mlflow.active_run() is None


def test_run_experiment__mlflow(
    capsys: CaptureFixture,
    config: Config,
    data_dir: Path,
    model_dir: Path,
    results_dir: Path,
    cache_dir: Path,
    mlflow_uri: str,
) -> None:
    config.model.train.max_epoch = 1
    config.eval.splits = ["train", "valid", "test"]

    run_experiment(
        config,
        data_dir,
        model_dir,
        results_dir,
        cache_dir,
        mlflow_uri,
        force_training=False,
        echo=False,
    )

    path = next(
        (path for path in Path(mlflow_uri).glob("0/*") if path.stem != "meta")
    )
    assert all(
        (path / "metrics" / file).exists()
        for file in (
            "train__accuracy",
            "valid__accuracy",
            "test__accuracy",
        )
    )
    assert (path / "artifacts" / "config.yml").exists()
    assert (path / "artifacts" / "train_results.json").exists()
    assert (path / "artifacts" / "loss_training_curves.svg").exists()
    assert (path / "artifacts" / "accuracy_training_curves.svg").exists()


def test_read_config(
    capsys: CaptureFixture, config_file: Path, config: Config
) -> None:
    config_ = read_config(config_file)
    assert config_ == config
    assert config_.name == config_file.stem
    assert not capsys.readouterr().out


def test_read_config__doesnt_exist(tmp_path: Path) -> None:
    with pytest.raises(FileNotFoundError):
        read_config(tmp_path / "test.yml")


def test_read_config__warning(
    capsys: CaptureFixture, config_file: Path, config: Config
) -> None:
    config.eval.data.seed = config.eval.data.seed * 2
    save_config(config, config_file)
    read_config(config_file)
    assert (
        "The same type of data was used for training and evaluation, "
        + "but with different seeds."
    ) in capsys.readouterr().out


def test_save_config(tmp_path: Path) -> None:
    path = tmp_path / "config" / "test_.yml"

    config = Config(name="test_")

    assert not path.exists()
    save_config(config, path)
    assert path.exists()

    yaml = YAML()
    config_ = yaml.load(path)
    assert "name" not in config_


############
# internal #
############


def test_add_comments(tmp_path: Path) -> None:
    config = Config(name="test")
    config_ = _add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 2
    assert len(config_) == 5
    assert "experiment" in config_.ca.items
    assert "hash" in config_.ca.items
    assert "model" in config_
    assert "eval" in config_


def test_add_comments__every_line_commented(tmp_path: Path) -> None:
    config = Config(name="test")
    save_config(config, tmp_path / "test.yml")

    with open(tmp_path / "test.yml", "r") as f:
        for line in f:
            if not any(
                line.strip().startswith(key)
                for key in ("model", "data", "train", "eval", "- ")
            ):
                assert (
                    re.search(r"# .+$", line) is not None
                ), f"Line {line} is missing a comment."


def test_init_mlflow(mlflow_uri: str) -> None:
    path = Path(mlflow_uri) / "0"
    config = Config(name="test1")

    _init_mlflow(config, mlflow_uri)
    id_ = mlflow.active_run().info.run_id
    mlflow.end_run()

    assert (path / id_).exists()
    assert len(list((path / id_ / "params").glob("*"))) > 10


def test_flatten_dict() -> None:
    dict_ = {"a": {"a": 1, "b": 2, "c": 3}, "b": {"a": {"a": 1}}}
    assert equals(
        _flatten_dict(dict_), {"a__a": 1, "a__b": 2, "a__c": 3, "b__a__a": 1}
    )


def test_log_mlflow_artifact__not_implemented(mlflow_uri: str) -> None:
    mlflow.set_tracking_uri(mlflow_uri)
    mlflow.start_run()
    with pytest.raises(NotImplementedError):
        _log_mlflow_artifact("test", "test.wrong")
    mlflow.end_run()
    assert mlflow.active_run() is None
