import pytest

from kt.data.utils import DATASETS, get_module


def test_datasets() -> None:
    assert DATASETS


@pytest.mark.parametrize("type_", DATASETS)
def test_get_module(type_: str) -> None:
    assert f"kt.data.datasets.{type_}" in str(get_module(type_))


def test_get_module__wrong() -> None:
    with pytest.raises(NotImplementedError):
        get_module("wrong")
