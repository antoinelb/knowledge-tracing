from pathlib import Path
from unittest.mock import Mock

import polars as pl
import pytest
from _pytest.capture import CaptureFixture
from pytest_mock import MockerFixture
from ruamel.yaml.comments import CommentedMap

from kt.data.datasets.synthetic_5 import (
    Config,
    add_comments,
    convert_data,
    download_data,
    read_data,
)
from tests.utils import equals

#########
# types #
#########


def test_type() -> None:
    assert Config().type == "synthetic_5"


############
# external #
############


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 0


@pytest.mark.parametrize("echo", [True, False])
def test_read_data(
    mocker: MockerFixture,
    capsys: CaptureFixture,
    root_dir: Path,
    echo: bool,
) -> None:
    data_dir = root_dir / "data"
    cache_dir = root_dir / "cache"
    config = Config()

    data_ = [[0, 1, 0], [1, 0, 1], [1, 1, 1], [0, 0, 0]]
    correct = pl.DataFrame(
        {
            "student": [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3],
            "question": [0, 1, 2] * 4,
            "timestamp": [0, 1, 2] * 4,
            "answer": [0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0],
        }
    )

    mocker.patch(
        "kt.data.datasets.synthetic_5.download_data", return_value=data_
    )
    data_1 = read_data(config, data_dir, cache_dir, echo=echo)
    assert isinstance(data_1, pl.DataFrame)
    assert equals(data_1, correct)
    if echo:
        output = capsys.readouterr().out
        assert "Converting synthetic 5 data..." in output
        assert "Saving cached synthetic 5 data..." in output
        assert "Reading cached synthetic 5 data..." not in output
        assert "Reading raw synthetic 5 data..." not in output
    else:
        assert capsys.readouterr().out == ""

    data_2 = read_data(config, data_dir, cache_dir, echo=echo)
    assert equals(data_1, data_2)
    if echo:
        output = capsys.readouterr().out
        assert "Converting synthetic 5 data..." not in output
        assert "Saving cached synthetic 5 data..." not in output
        assert "Reading cached synthetic 5 data..." in output
        assert "Reading raw synthetic 5 data..." not in output
    else:
        assert capsys.readouterr().out == ""

    (cache_dir / f"{config.hash}.ipc").unlink()
    data_3 = read_data(config, data_dir, cache_dir, echo=echo)
    assert equals(data_1, data_3)
    if echo:
        output = capsys.readouterr().out
        assert "Converting synthetic 5 data..." not in output
        assert "Saving cached synthetic 5 data..." in output
        assert "Reading cached synthetic 5 data..." not in output
        assert "Reading raw synthetic 5 data..." in output
    else:
        assert capsys.readouterr().out == ""

    (cache_dir / f"{config.hash}.ipc").unlink()
    cache_dir.rmdir()
    read_data(config, data_dir, cache_dir, echo=echo)

    (cache_dir / f"{config.hash}.ipc").unlink()
    (data_dir / "synthetic_5" / "data.csv").unlink()
    read_data(config, data_dir, cache_dir, echo=echo)


############
# internal #
############


@pytest.mark.parametrize("echo", [True, False])
def test_download_data(
    mocker: MockerFixture, capsys: CaptureFixture, echo: bool
) -> None:
    correct = [line for _ in range(50) for line in ([1, 0, 1], [0, 1, 0])]
    data = (b"1,0,1\n" if i % 2 == 0 else b"0,1,0\n" for i in range(100))
    resp = Mock()
    resp.iter_bytes.return_value = data

    stream = mocker.patch("kt.data.datasets.synthetic_5.httpx.stream")
    stream.return_value.__enter__.return_value = resp
    assert download_data(echo=echo) == correct

    if echo:
        output = capsys.readouterr().out
        assert "Connecting to server..." in output
        assert "Downloading data..." in output
        assert "Reading downloaded data..." in output
    else:
        assert capsys.readouterr().out == ""

    (Path("/tmp") / "synthetic_5.csv").unlink()


def test_download_data__no_data(
    mocker: MockerFixture, capsys: CaptureFixture
) -> None:
    data = (b"",)
    resp = Mock()
    resp.iter_bytes.return_value = data

    stream = mocker.patch("kt.data.datasets.synthetic_5.httpx.stream")
    stream.return_value.__enter__.return_value = resp
    assert download_data() == []

    output = capsys.readouterr().out
    assert "Connecting to server..." in output
    assert "Downloading data..." not in output
    assert "Reading downloaded data..." in output

    (Path("/tmp") / "synthetic_5.csv").unlink()


def test_convert_data() -> None:
    data_ = [[0, 1, 0], [1, 0, 1], [1, 1, 1], [0, 0, 0]]
    correct = pl.DataFrame(
        {
            "student": [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3],
            "question": [0, 1, 2] * 4,
            "timestamp": [0, 1, 2] * 4,
            "answer": [0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0],
        }
    )
    assert equals(convert_data(data_), correct)
