import random
import time
from pathlib import Path
from typing import Iterator
from unittest.mock import Mock

import jax.numpy as jnp
import polars as pl
import pytest
from _pytest.capture import CaptureFixture
from pytest_mock import MockerFixture
from ruamel.yaml.comments import CommentedMap

from kt.data.data import (
    Config,
    Data,
    _convert_to_sequences,
    _create_batch,
    _create_question2index,
    _read_data,
    _split_data,
    add_comments,
    convert_data,
    parse_config,
    read_data,
)
from kt.data.datasets import synthetic_5
from kt.data.utils import DATASETS
from tests.utils import equals

#########
# types #
#########


def test_data__shuffle(fake_data: Data) -> None:
    # checks shuffle works
    for i in range(10):
        data_ = fake_data.shuffle(i)
        for batch, batch_ in zip(fake_data, data_):
            if not equals(batch, batch_):
                break
        else:
            continue
        break
    else:
        assert 0

    # check same seed gives same shuffle
    for i in range(10):
        data_ = fake_data.shuffle(i)
        other_data = fake_data.shuffle(i)
        for batch, batch_ in zip(other_data, data_):
            if not equals(batch, batch_):
                assert 0

    # check different seed gives different shuffle
    for i in range(10):
        data_ = fake_data.shuffle(i)
        other_data = fake_data.shuffle(i + 1)
        for batch, batch_ in zip(other_data, data_):
            if not equals(batch, batch_):
                break
        else:
            continue
        break
    else:
        assert 0


############
# external #
############


@pytest.mark.parametrize("type_", DATASETS)
def test_parse_config(type_: str) -> None:
    if type_ == "synthetic_5":
        assert isinstance(
            parse_config({"type": type_}),
            synthetic_5.Config,
        )
        assert isinstance(
            parse_config(synthetic_5.Config()),
            synthetic_5.Config,
        )
    else:
        assert 0


def test_parse_config__no_type_() -> None:
    assert equals(parse_config({}), synthetic_5.Config())
    assert equals(parse_config(), synthetic_5.Config())
    assert equals(parse_config(None), synthetic_5.Config())


def test_parse_config__wrong(mocker: MockerFixture) -> None:
    module = Mock()
    module.Config.side_effect = AttributeError()
    mocker.patch("kt.data.data.get_module", return_value=module)
    with pytest.raises(NotImplementedError):
        parse_config({"type": "wrong"})


def test_add_comments() -> None:
    config = Config()
    config_ = add_comments(config)
    assert isinstance(config_, CommentedMap)
    assert len(config_.ca.items) == 9
    assert len(config_) == 9
    assert "type" in config_.ca.items
    assert "user_field" in config_.ca.items
    assert "timestamp_field" in config_.ca.items
    assert "question_field" in config_.ca.items
    assert "answer_field" in config_.ca.items
    assert "p_valid" in config_.ca.items
    assert "p_test" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items


def test_add_comments__wrong(mocker: MockerFixture) -> None:
    module = Mock()
    module.add_comments.side_effect = AttributeError()
    mocker.patch("kt.data.data.get_module", return_value=module)
    config = Config()
    with pytest.raises(NotImplementedError):
        add_comments(config)


@pytest.mark.parametrize("echo", [True, False])
def test_read_data(
    capsys: CaptureFixture, mocker: MockerFixture, root_dir: Path, echo: bool
) -> None:
    data_dir = root_dir / "data"
    cache_dir = root_dir / "cache"
    config = Config(
        user_field="a",
        timestamp_field="b",
        answer_field="c",
        p_valid=0.2,
        p_test=0.1,
    )
    data = pl.DataFrame(
        {
            "a": [i for i in range(10) for _ in range(10)],
            "b": [i for i in range(10) for _ in range(10)],
            "c": list(range(100)),
        }
    )
    _read_data = mocker.patch("kt.data.data._read_data", return_value=data)

    start = time.time()
    split_data = read_data(config, data_dir, cache_dir, echo=echo)
    time_1 = time.time() - start
    assert split_data.shape[0] == data.shape[0]
    assert set(split_data["dataset"].unique().to_list()) == {
        "train",
        "valid",
        "test",
    }

    start = time.time()
    split_data_ = read_data(config, data_dir, cache_dir, echo=echo)
    time_2 = time.time() - start
    assert equals(split_data, split_data_)
    assert time_2 < time_1

    assert _read_data.call_count == 1

    if echo:
        assert capsys.readouterr().out
    else:
        assert not capsys.readouterr().out


@pytest.mark.parametrize("echo", [True, False])
def test_convert_data(
    capsys: CaptureFixture,
    mocker: MockerFixture,
    root_dir: Path,
    fake_df: pl.DataFrame,
    echo: bool,
) -> None:
    cache_dir = root_dir / "cache"
    config = Config(
        user_field="a",
        timestamp_field="t",
        question_field="b",
        answer_field="c",
    )

    correct = [
        {
            "user": jnp.array([5, 0, 2, 4]),
            "questions": jnp.array(
                [
                    [0, 2, 3],
                    [1, 3, 2],
                    [2, 1, 3],
                    [3, 2, 1],
                ]
            ),
            "answers": jnp.array(
                [
                    [0, 1, 1],
                    [0, 1, 0],
                    [1, 0, 1],
                    [0, 1, 0],
                ]
            ),
            "prev_answers": jnp.array(
                [
                    [0, 0, 1],
                    [0, 0, 1],
                    [0, 1, 0],
                    [0, 0, 1],
                ]
            ),
        },
        {
            "user": jnp.array([3, 1]),
            "questions": jnp.array(
                [
                    [0, 0, 6, 5, 4, 3],
                    [1, 2, 3, 4, 5, 6],
                ]
            ),
            "answers": jnp.array(
                [
                    [0, 0, 0, 1, 0, 1],
                    [1, 0, 1, 0, 1, 0],
                ]
            ),
            "prev_answers": jnp.array(
                [
                    [0, 0, 0, 0, 1, 0],
                    [0, 1, 0, 1, 0, 1],
                ]
            ),
        },
    ]
    flat_data = pl.DataFrame(
        {
            "a": [
                u
                for u, seq_len in zip(fake_df["a"], fake_df["seq_len"])
                for _ in range(seq_len)
            ],
            "t": [xx for x in fake_df["t"] for xx in x],
            "b": [xx for x in fake_df["b"] for xx in x],
            "c": [xx for x in fake_df["c"] for xx in x],
        }
    )

    data, question2index = convert_data(
        flat_data, config, 4, cache_dir, echo=echo
    )
    assert question2index == {i: i for i in range(1, 7)}
    assert isinstance(data, Data)
    assert isinstance(iter(data), Iterator)
    assert len(data) == 2
    i = 0
    for d, c in zip(data, correct):
        i += 1
        assert len(d) == 4
        for key in ("user", "questions", "answers", "prev_answers"):
            assert jnp.array_equal(d[key], c[key])
    assert i == 2

    data, question2index = convert_data(
        flat_data,
        config,
        4,
        cache_dir,
        question2index={i: i * 2 for i in range(1, 7)},
        echo=echo,
    )
    assert question2index == {i: i * 2 for i in range(1, 7)}
    for d, c in zip(data, correct):
        assert jnp.array_equal(d["questions"], c["questions"] * 2)

    if echo:
        output = capsys.readouterr().out
        assert "Converting questions to indices..." in output
        assert "Converting data to sequences..." in output
        assert "Saving cached sequence data..." not in output
        assert "Reading cached sequence data..." not in output
    else:
        assert not capsys.readouterr().out


@pytest.mark.flaky(reruns=3)
@pytest.mark.parametrize("echo", [True, False])
def test_convert_data__cache(
    capsys: CaptureFixture,
    mocker: MockerFixture,
    root_dir: Path,
    fake_df: pl.DataFrame,
    echo: bool,
) -> None:
    cache_dir = root_dir / "cache"
    config = Config(
        user_field="a",
        timestamp_field="t",
        question_field="b",
        answer_field="c",
    )

    flat_data = pl.DataFrame(
        {
            "a": [
                u
                for u, seq_len in zip(fake_df["a"], fake_df["seq_len"])
                for _ in range(seq_len)
            ],
            "t": [xx for x in fake_df["t"] for xx in x],
            "b": [xx for x in fake_df["b"] for xx in x],
            "c": [xx for x in fake_df["c"] for xx in x],
            "dataset": [
                "a" if i % 2 == 0 else "b"
                for i, seq_len in enumerate(fake_df["seq_len"])
                for _ in range(seq_len)
            ],
        }
    )

    start = time.time()
    data, question2index = convert_data(
        flat_data, config, 4, cache_dir, echo=echo
    )
    time_1 = time.time() - start

    if echo:
        output = capsys.readouterr().out
        assert "Converting questions to indices..." in output
        assert "Converting data to sequences..." in output
        assert "Saving cached sequence data..." in output
        assert "Reading cached sequence data..." not in output
    else:
        assert not capsys.readouterr().out

    start = time.time()
    data, question2index = convert_data(
        flat_data, config, 4, cache_dir, echo=echo
    )
    time_2 = time.time() - start

    assert time_2 < time_1

    if echo:
        output = capsys.readouterr().out
        assert "Converting questions to indices..." not in output
        assert "Converting data to sequences..." not in output
        assert "Saving cached sequence data..." not in output
        assert "Reading cached sequence data..." in output
    else:
        assert not capsys.readouterr().out


############
# internal #
############


@pytest.mark.parametrize("type_", DATASETS)
@pytest.mark.parametrize("echo", [True, False])
def test_read_data_(
    mocker: MockerFixture, root_dir: Path, type_: str, echo: bool
) -> None:
    data_dir = root_dir / "data"
    cache_dir = root_dir / "cache"
    _read_data = mocker.patch(f"kt.data.datasets.{type_}.read_data")
    config = parse_config({"type": type_})
    _read_data(config, data_dir, cache_dir, echo=echo)
    _read_data.assert_called_with(config, data_dir, cache_dir, echo=echo)


def test_read_data__not_implemented(
    mocker: MockerFixture, root_dir: Path
) -> None:
    data_dir = root_dir / "data"
    cache_dir = root_dir / "cache"
    config = parse_config({"type": "synthetic_5"})
    module = Mock()
    module.read_data.side_effect = AttributeError()
    mocker.patch("kt.data.data.get_module", return_value=module)
    with pytest.raises(NotImplementedError):
        _read_data(config, data_dir, cache_dir)


@pytest.mark.parametrize(
    "p_train,p_valid,p_test",
    [
        (1, 0, 0),
        (0, 1, 0),
        (0, 0, 1),
        (0.85, 0.1, 0.05),
        (0.95, 0, 0.05),
        (0.9, 0.1, 0),
    ],
)
@pytest.mark.parametrize("echo", [True, False])
def test_split_data(
    capsys: CaptureFixture,
    p_train: float,
    p_valid: float,
    p_test: float,
    echo: bool,
) -> None:
    config = Config(
        user_field="a",
        p_valid=p_valid,
        p_test=p_test,
        seed=random.randint(0, 100_000),
    )
    n = 100
    data = pl.DataFrame(
        {
            "a": [i for i in range(n) for j in range(i + 1)],
            "b": list(range(n * (n + 1) // 2)),
        }
    )

    split_data = _split_data(data, config, echo=echo)
    train_users = set(
        split_data.filter(pl.col("dataset") == "train")["a"].to_list()
    )
    valid_users = set(
        split_data.filter(pl.col("dataset") == "valid")["a"].to_list()
    )
    test_users = set(
        split_data.filter(pl.col("dataset") == "test")["a"].to_list()
    )
    assert len(train_users) == p_train * n
    assert len(valid_users) == p_valid * n
    assert len(test_users) == p_test * n
    assert len(train_users) + len(valid_users) + len(test_users) == n


def test_create_question2index() -> None:
    data = pl.DataFrame({"q": [0, 1, 2, 3, 2, 1]})
    assert _create_question2index(data, "q") == {0: 1, 1: 2, 2: 3, 3: 4}


def test_convert_to_sequences() -> None:
    data = pl.DataFrame(
        {
            "a": [0, 1, 2] * 3,
            "b": [0, 1, 2, 2, 0, 1, 1, 2, 0],
            "c": [1, 1, 1, 2, 2, 2, 3, 3, 3],
            "d": [1, 2, 3, 4, 5, 6, 7, 8, 9],
        }
    )
    correct = pl.DataFrame(
        {
            "a": [0, 1, 2],
            "seq_len": [3, 3, 3],
            "b": [[0, 1, 2]] * 3,
            "c": [[1, 3, 2], [2, 1, 3], [3, 2, 1]],
            "d": [
                [1, 7, 4],
                [5, 2, 8],
                [9, 6, 3],
            ],
        }
    )
    result = _convert_to_sequences(data, by="a", timestamp="b")
    assert equals(result.sort("a"), correct.sort("a"))

    data = pl.DataFrame(
        {
            "a": [0, 1, 2] * 3,
            "b": [0, 1, 2, 2, 0, 1, 1, 2, 0],
            "c": [1, 1, 1, 2, 2, 2, 3, 3, 3],
            "d": [1, 2, 3, 4, 5, 6, 7, 8, 9],
            "dataset": ["train"] * 3 + ["test"] * 3 + ["train"] * 3,
        }
    )
    correct = pl.DataFrame(
        {
            "dataset": ["test"] * 3 + ["train"] * 3,
            "a": [0, 1, 2, 0, 1, 2],
            "seq_len": [1, 1, 1, 2, 2, 2],
            "b": [[2], [0], [1], [0, 1], [1, 2], [0, 2]],
            "c": [[2], [2], [2], [1, 3], [1, 3], [3, 1]],
            "d": [[4], [5], [6], [1, 7], [2, 8], [9, 3]],
        }
    )
    result = _convert_to_sequences(data, by="a", timestamp="b")
    assert equals(
        result.sort(["dataset", "a"]), correct.sort(["dataset", "a"])
    )


def test_create_batch() -> None:
    data = pl.DataFrame(
        {
            "user": [0, 1, 2],
            "questions": [[1], [1, 2], [1, 2, 3]],
            "answers": [[1], [1, 0], [1, 0, 1]],
            "seq_len": [1, 2, 3],
        }
    )
    assert equals(
        _create_batch(data),
        {
            "user": jnp.array([0, 1, 2]),
            "questions": jnp.array([[0, 0, 1], [0, 1, 2], [1, 2, 3]]),
            "answers": jnp.array([[0, 0, 1], [0, 1, 0], [1, 0, 1]]),
            "prev_answers": jnp.array([[0, 0, 0], [0, 0, 1], [0, 1, 0]]),
        },
    )
