from math import log

import jax.numpy as jnp
import pytest
from pytest_mock import MockerFixture

from kt.utils.metrics import (
    Metric,
    _accuracy,
    _auc,
    _max_time,
    _mean_time,
    _roc_curve,
    _sigmoid_crossentropy,
    _std_time,
    get_metric_fct,
)

############
# external #
############


@pytest.mark.parametrize("metric", Metric.__args__)  # type: ignore
def test_get_metric(mocker: MockerFixture, metric: str) -> None:
    metric_ = mocker.patch(f"kt.utils.metrics._{metric}")
    inputs = {str(i): jnp.array([i]) for i in range(3)}
    fct = get_metric_fct(metric)  # type: ignore
    fct(inputs)
    metric_.assert_called_once_with(inputs)


def test_get_metric__wrong() -> None:
    with pytest.raises(NotImplementedError):
        get_metric_fct("wrong")  # type: ignore


############
# internal #
############


def test_sigmoid_crossentropy() -> None:
    predictions = jnp.array([[0, 0.25, 0.5, 0.75], [0, 0.75, 0.5, 0.25]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 0]])
    correct = -(log(0.75) * 4 + log(0.5) * 2) / 6

    assert (
        _sigmoid_crossentropy(
            {
                "predictions": predictions,
                "questions": questions,
                "answers": answers,
            }
        )
        == correct
    )


def test_sigmoid_crossentropy__missing_inputs() -> None:
    predictions = jnp.array([[0, 0.25, 0.5, 0.75], [0, 0.75, 0.5, 0.25]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 0]])

    with pytest.raises(KeyError):
        _sigmoid_crossentropy(
            {
                "questions": questions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _sigmoid_crossentropy(
            {
                "predictions": predictions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _sigmoid_crossentropy(
            {
                "predictions": predictions,
                "questions": questions,
            }
        )


def test_accuracy() -> None:
    predictions = jnp.array([[0.5, 0.25, 0.75], [0.25, 0.5, 0.51]])
    answers = jnp.array([[0, 0, 1], [0, 1, 1]])
    questions = jnp.array([[0, 1, 1], [0, 1, 1]])
    assert (
        _accuracy(
            {
                "predictions": predictions,
                "questions": questions,
                "answers": answers,
            }
        )
        == 0.75
    )


def test_accuracy__missing_inputs() -> None:
    predictions = jnp.array([[0, 0.25, 0.5, 0.75], [0, 0.75, 0.5, 0.25]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 0]])

    with pytest.raises(KeyError):
        _accuracy(
            {
                "questions": questions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _accuracy(
            {
                "predictions": predictions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _accuracy(
            {
                "predictions": predictions,
                "questions": questions,
            }
        )


def test_auc() -> None:
    predictions = jnp.array([[0, 0, 0.4, 0.8], [0, 0.8, 0.4, 1]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 1]])
    correct = (
        (0 + 0.25) * 0 / 2
        + (0.25 + 0.75) * 0 / 2
        + (0.75 + 1) * 0.5 / 2
        + (1 + 1) * 0.5 / 2
    )
    assert (
        _auc(
            {
                "predictions": predictions,
                "questions": questions,
                "answers": answers,
            }
        )
        == correct
    )


def test_auc__missing_inputs() -> None:
    predictions = jnp.array([[0, 0.25, 0.5, 0.75], [0, 0.75, 0.5, 0.25]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 0]])

    with pytest.raises(KeyError):
        _auc(
            {
                "questions": questions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _auc(
            {
                "predictions": predictions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _auc(
            {
                "predictions": predictions,
                "questions": questions,
            }
        )


def test_roc_curve() -> None:
    predictions = jnp.array([[0, 0, 0.4, 0.8], [0, 0.8, 0.4, 1]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 1]])
    correct_tpr = jnp.array([0, 1, 3, 4, 4]) / 4
    correct_fpr = jnp.array([0, 0, 0, 1, 2]) / 2
    correct_thresholds = jnp.array([2, 1, 0.8, 0.4, 0])

    tpr, fpr, thresholds = _roc_curve(
        {
            "predictions": predictions,
            "questions": questions,
            "answers": answers,
        }
    )
    assert jnp.array_equal(tpr, correct_tpr)
    assert jnp.array_equal(fpr, correct_fpr)
    assert jnp.array_equal(thresholds, correct_thresholds)


def test_roc_curve__missing_inputs() -> None:
    predictions = jnp.array([[0, 0.25, 0.5, 0.75], [0, 0.75, 0.5, 0.25]])
    questions = jnp.array([[0, 1, 1, 1], [0, 1, 1, 1]])
    answers = jnp.array([[0, 0, 0, 1], [0, 1, 1, 0]])

    with pytest.raises(KeyError):
        _roc_curve(
            {
                "questions": questions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _roc_curve(
            {
                "predictions": predictions,
                "answers": answers,
            }
        )
    with pytest.raises(KeyError):
        _roc_curve(
            {
                "predictions": predictions,
                "questions": questions,
            }
        )


def test_time_metrics() -> None:
    inputs = {"time": jnp.array([0.25, 0.5, 0.75])}
    assert _mean_time(inputs).item() == 0.5
    assert abs(_std_time(inputs).item() - (2 * 0.25**2 / 3) ** 0.5) < 1e-8
    assert _max_time(inputs).item() == 0.75
