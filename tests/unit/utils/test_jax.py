from pathlib import Path
from typing import Any

import jax.numpy as jnp
import pytest

from kt.utils.jax import (
    _convert_from_tree,
    _convert_to_tree,
    get_shapes,
    read_tree,
    save_tree,
)
from tests.utils import equals

############
# external #
############


@pytest.mark.parametrize(
    "tree",
    [
        [jnp.array([1])],
        [jnp.array([1]), jnp.array([2])],
        [jnp.array([1]), [jnp.array([2])]],
        [
            jnp.array([1]),
            [jnp.array([2]), jnp.array([3])],
            [[jnp.array([4])]],
        ],
        [[jnp.array([j]) for j in range(i + 1)] for i in range(11)],
    ],
)
def test_read_and_save_tree(tmp_path: Path, tree: list[Any]) -> None:
    path = tmp_path / "test"
    save_tree(tree, path)
    tree_ = read_tree(path)
    assert equals(tree, tree_)


@pytest.mark.parametrize(
    "data,correct",
    [
        (0, 0),
        (jnp.array(0), ()),
        (jnp.array([1, 2]), (2,)),
        (jnp.array([[1, 2], [3, 4], [5, 6]]), (3, 2)),
        (
            (
                jnp.array(0),
                jnp.array([1, 2]),
                jnp.array([[1, 2], [3, 4], [5, 6]]),
            ),
            ((), (2,), (3, 2)),
        ),
        (
            [
                jnp.array(0),
                jnp.array([1, 2]),
                jnp.array([[1, 2], [3, 4], [5, 6]]),
            ],
            [(), (2,), (3, 2)],
        ),
        (
            {
                "a": jnp.array(0),
                "b": jnp.array([1, 2]),
                "c": jnp.array([[1, 2], [3, 4], [5, 6]]),
            },
            {"a": (), "b": (2,), "c": (3, 2)},
        ),
        (
            [
                ("a", jnp.array(0)),
                ("b", jnp.array([1, 2])),
                ("c", jnp.array([[1, 2], [3, 4], [5, 6]])),
            ],
            [("a", ()), ("b", (2,)), ("c", (3, 2))],
        ),
    ],
)
def test_get_shapes(data: Any, correct: Any) -> None:
    assert equals(get_shapes(data), correct)


############
# internal #
############


@pytest.mark.parametrize(
    "tree,shapes,structure",
    [
        (jnp.array(0), [()], "*"),
        (jnp.arange(3), [(3,)], "*"),
        ([jnp.arange(2), jnp.arange(3)], [(2,), (3,)], "[*, *]"),
        (
            {"a": jnp.arange(2), "b": jnp.arange(3)},
            [(2,), (3,)],
            "{'a': *, 'b': *}",
        ),
        (
            {"a": [jnp.arange(2)], "b": jnp.arange(3)},
            [(2,), (3,)],
            "{'a': [*], 'b': *}",
        ),
    ],
)
def test_convert_from_tree(tree: Any, shapes: Any, structure: str) -> None:
    arrays, structure_ = _convert_from_tree(tree)
    assert equals(get_shapes(arrays), shapes)
    assert structure == structure_


@pytest.mark.parametrize(
    "tree,arrays,structure",
    [
        (jnp.array(0), [jnp.array(0)], "*"),
        (jnp.arange(3), [jnp.arange(3)], "*"),
        (
            [jnp.arange(2), jnp.arange(3)],
            [jnp.arange(2), jnp.arange(3)],
            "[*, *]",
        ),
        (
            (jnp.arange(2), jnp.arange(3)),
            [jnp.arange(2), jnp.arange(3)],
            "(*, *)",
        ),
        (
            {"a": jnp.arange(2), "b": jnp.arange(3)},
            [jnp.arange(2), jnp.arange(3)],
            "{'a': *, 'b': *}",
        ),
        (
            {"a": [jnp.arange(2)], "b": jnp.arange(3)},
            [jnp.arange(2), jnp.arange(3)],
            "{'a': [*], 'b': *}",
        ),
        (
            [[jnp.arange(2), jnp.arange(3)], jnp.arange(4)],
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "[[*, *], *]",
        ),
        (
            ((jnp.arange(2), jnp.arange(3)), jnp.arange(4)),
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "((*, *), *)",
        ),
        (
            {
                "a": {"b": jnp.arange(2), "c": jnp.arange(3)},
                "d": jnp.arange(4),
            },
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "{'a': {'b': *, 'c': *}, 'd': *}",
        ),
        (
            [(jnp.arange(2), jnp.arange(3), jnp.arange(4))],
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "[(*, *, *)]",
        ),
        (
            ([jnp.arange(2), jnp.arange(3), jnp.arange(4)],),
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "([*, *, *])",
        ),
        (
            [{"a": jnp.arange(2), "b": jnp.arange(3), "c": jnp.arange(4)}],
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "[{'a': *, 'b': *, 'c': *}]",
        ),
        (
            ({"a": jnp.arange(2), "b": jnp.arange(3), "c": jnp.arange(4)},),
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "({'a': *, 'b': *, 'c': *})",
        ),
        (
            {"a": [jnp.arange(2), jnp.arange(3), jnp.arange(4)]},
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "{'a': [*, *, *]}",
        ),
        (
            {"a": (jnp.arange(2), jnp.arange(3), jnp.arange(4))},
            [jnp.arange(2), jnp.arange(3), jnp.arange(4)],
            "{'a': (*, *, *)}",
        ),
    ],
)
def test_convert_to_tree(
    tree: Any, arrays: list[jnp.ndarray], structure: str
) -> None:
    tree_ = _convert_to_tree(arrays, structure)
    assert equals(tree, tree_)


def test_convert_to_tree__wrong() -> None:
    with pytest.raises(NotImplementedError):
        _convert_to_tree([jnp.arange(1)], "'*'")
