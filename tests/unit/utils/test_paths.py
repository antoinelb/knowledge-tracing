from pathlib import Path

from pytest_mock import MockerFixture

from kt.utils.paths import determine_path, root_dir


def test_root_dir() -> None:
    assert ".git" in [path.stem for path in root_dir.glob(".*")]


def test_determine_path__relative(
    mocker: MockerFixture, root_dir: Path
) -> None:
    mocker.patch("kt.utils.paths.root_dir", root_dir)

    path = determine_path("test")
    assert str(path).startswith(str(root_dir))
    assert path.name == "test"

    path = determine_path("./test")
    assert str(path).startswith(str(root_dir))
    assert path.name == "test"


def test_determine_path__absolute() -> None:
    path = determine_path("/tmp/test")
    assert str(path) == "/tmp/test"


def test_determine_path__home() -> None:
    path = determine_path("~/test")
    assert "~" not in str(path)
    assert str(path).startswith("/")
    assert path.name == "test"
