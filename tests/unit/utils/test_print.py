import io

import pytest
from _pytest.capture import CaptureFixture
from tqdm import tqdm

from kt.utils.print import (
    done_print,
    format_list,
    format_time,
    load_print,
    load_progress,
    warning_print,
)


def test_load_print(capsys: CaptureFixture) -> None:
    load_print("test")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[*]" in output
    assert "test" in output
    assert not output.endswith("\n")
    assert output.index("[*]") < output.index("test")

    load_print("test", "1/2")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[1/2]" in output
    assert "[*]" not in output
    assert "test" in output
    assert not output.endswith("\n")
    assert output.index("[1/2]") < output.index("test")

    load_print("test", echo=False)
    assert not capsys.readouterr().out


def test_done_print(capsys: CaptureFixture) -> None:
    done_print("test")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[+]" in output
    assert "test" in output
    assert output.endswith("\n")
    assert output.index("[+]") < output.index("test")

    done_print("test", "1/2")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[1/2]" in output
    assert "[+]" not in output
    assert "test" in output
    assert output.endswith("\n")
    assert output.index("[1/2]") < output.index("test")

    done_print("test", echo=False)
    assert not capsys.readouterr().out


def test_warning_print(capsys: CaptureFixture) -> None:
    warning_print("test")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[!]" in output
    assert "test" in output
    assert output.endswith("\n")
    assert output.index("[!]") < output.index("test")

    warning_print("test", "1/2")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[1/2]" in output
    assert "[!]" not in output
    assert "test" in output
    assert output.endswith("\n")
    assert output.index("[1/2]") < output.index("test")

    warning_print("test", echo=False)
    assert not capsys.readouterr().out


def test_load_progress() -> None:
    with io.StringIO() as output_:
        iter_ = load_progress(range(10), "test", file=output_)
        output = output_.getvalue()
        assert isinstance(iter_, tqdm)
        assert "[*]" in output
        assert "test" in output
        assert output.index("[*]") < output.index("test")

    with io.StringIO() as output_:
        iter_ = load_progress(range(10), "test", "1/2", file=output_)
        output = output_.getvalue()
        assert isinstance(iter_, tqdm)
        assert "[1/2]" in output
        assert "[*]" not in output
        assert "test" in output
        assert output.index("[1/2]") < output.index("test")

    with io.StringIO() as output_:
        iter_ = load_progress(range(10), "test", echo=False, file=output_)
        output = output_.getvalue()
        assert not isinstance(iter_, tqdm)


@pytest.mark.parametrize(
    "list_,surround,correct",
    [
        ([], "", ""),
        (["a"], "", "a"),
        (["a", "b"], "", "a and b"),
        (["a", "b", "c"], "", "a, b and c"),
        (["a", "b", "c", "d"], "", "a, b, c and d"),
        ([], "`", ""),
        (["a"], "`", "`a`"),
        (["a", "b"], "`", "`a` and `b`"),
        (["a", "b", "c"], "`", "`a`, `b` and `c`"),
        (["a", "b", "c", "d"], "`", "`a`, `b`, `c` and `d`"),
        ([], '"', ""),
        (["a"], '"', '"a"'),
        (["a", "b"], '"', '"a" and "b"'),
        (["a", "b", "c"], '"', '"a", "b" and "c"'),
        (["a", "b", "c", "d"], '"', '"a", "b", "c" and "d"'),
    ],
)
def test_format_list(list_: list[str], surround: str, correct: str) -> None:
    assert format_list(list_, surround) == correct


@pytest.mark.parametrize(
    "time_,correct",
    [
        (0, "0s"),
        (1, "1s"),
        (20, "20s"),
        (1.1, "1s"),
        (1.9, "2s"),
        (1.5, "2s"),
        (60, "1m0s"),
        (65, "1m5s"),
        (65.9, "1m6s"),
        (3600, "1h0m0s"),
        (3660, "1h1m0s"),
        (3720, "1h2m0s"),
        (3725, "1h2m5s"),
        (3725.9, "1h2m6s"),
    ],
)
def test_format_time(time_: float, correct: str) -> None:
    assert format_time(time_) == correct
