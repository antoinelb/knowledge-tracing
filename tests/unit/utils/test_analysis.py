import jax
import polars as pl

from kt.data import Data
from kt.model.models import ModelFcts
from kt.utils.analysis import convert_predictions


def test_convert_predictions(tiny_dkt: ModelFcts, fake_data: Data) -> None:
    init, predict, _ = tiny_dkt
    predict = jax.vmap(predict, [None, None, 0, None], 0)
    params = init(jax.random.PRNGKey(0), fake_data.n_questions)

    predictions = [
        data | predict(params, jax.random.PRNGKey(0), data, False)
        for data in fake_data
    ]
    predictions_ = convert_predictions(predictions)
    assert isinstance(predictions_, pl.DataFrame)
    assert all(
        key in predictions_.columns
        for key in ("user", "questions", "answers", "predictions")
    )
    for row in predictions_.rows():
        assert isinstance(row[0], int)
        assert all(
            len(row[1]) == len(row[i])
            for i in range(2, len(predictions_.columns))
        )
