from typing import Any, Dict, Optional

import pytest
from pydantic import BaseModel, ValidationError

from kt.utils.validators import (
    _hash_config,
    _prepare_config_for_hashing,
    hash_config,
    parse_config,
    set_field,
    set_seed_if_missing,
    verify_between_0_and_1,
)

############
# external #
############


def test_hash_config() -> None:
    class A(BaseModel):
        a: str
        b: int
        hash: str = ""

        _hash_config = hash_config()

    a = A(a="a", b=1)
    assert a.hash != ""

    b = A(b=1, a="a")
    assert b.hash == a.hash

    c = A(a="a", b=1, hash="abc")
    assert c.hash == a.hash

    d = A(a="b", b=1)
    assert d.hash != a.hash


def test_parse_config() -> None:
    class B(BaseModel):
        a: str = "a"
        b: int = 1

    def parse_config_(config: Optional[Dict[str, Any]] = None) -> B:
        if config is None:
            return B()
        elif config["a"] == "a":
            return B(**config)
        else:
            return B(**{**config, "b": 2})

    class A(BaseModel):
        a: Optional[B] = parse_config_()

        _parse_a = parse_config("a", parse_config_)

    a = A()
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 1  # type: ignore

    a = A(a=None)
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 1  # type: ignore

    a = A(a={"a": "a", "b": 1})
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 1  # type: ignore

    a = A(a={"a": "a", "b": 3})
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 3  # type: ignore

    a = A(a={"a": "b", "b": 3})
    assert a.a.a == "b"  # type: ignore
    assert a.a.b == 2  # type: ignore

    a = A(a=B(a="b", b=3))
    assert a.a.a == "b"  # type: ignore
    assert a.a.b == 3  # type: ignore


def test_set_field() -> None:
    class A(BaseModel):
        a: str = "a"

        _parse_a = set_field("a", "b")

    a = A()
    assert a.a == "b"

    a = A(a="c")
    assert a.a == "b"


def test_verify_between_0_and_1() -> None:
    class A(BaseModel):
        a: float = 0.5

        _parse_a = verify_between_0_and_1("a")

    A()
    A(a=0)
    A(a=1)
    A(a=0.5)

    with pytest.raises(ValidationError) as exc:
        A(a=-1)
    assert "The value of `a` must be between 0 and 1." in str(exc.value)
    with pytest.raises(ValidationError) as exc:
        A(a=1.5)
    assert "The value of `a` must be between 0 and 1." in str(exc.value)


def test_set_seed_if_missing() -> None:
    class A(BaseModel):
        a: int = 0

        _parse_a = set_seed_if_missing("a")

    assert isinstance(A().a, int)
    assert isinstance(A(a=0).a, int)
    for _ in range(10):
        assert A(a=123).a == 123


############
# internal #
############


def test_hash_config_() -> None:
    config = {"a": 1, "c": 3, "b": 2}
    assert _hash_config(config) == _hash_config(config)
    assert _hash_config(config) == _hash_config(
        {k: config[k] for k in sorted(config)}
    )
    assert _hash_config(config) != _hash_config({"a": 1})


def test_prepapre_config_for_hashing() -> None:
    config = {"a": [1, {"hash": ""}], "c": 3, "b": {"hash": ""}, "hash": ""}
    correct = {"a": [1, {}], "b": {}, "c": 3}
    assert _prepare_config_for_hashing(config) == correct
