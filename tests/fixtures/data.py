from pathlib import Path

import polars as pl
import pytest
from pytest_mock import MockerFixture

from kt.data import Config, Data, convert_data, read_data


@pytest.fixture
def fake_df() -> pl.DataFrame:
    return pl.DataFrame(
        {
            "a": [0, 1, 2, 3, 4, 5],
            "seq_len": [3, 6, 3, 4, 3, 2],
            "t": [
                [0, 1, 2],
                [5, 4, 3, 2, 1, 0],
                [0, 1, 2],
                [3, 2, 1, 0],
                [0, 1, 2],
                [1, 0],
            ],
            "b": [
                [1, 3, 2],
                [6, 5, 4, 3, 2, 1],
                [2, 1, 3],
                [3, 4, 5, 6],
                [3, 2, 1],
                [3, 2],
            ],
            "c": [
                [0, 1, 0],
                [0, 1, 0, 1, 0, 1],
                [1, 0, 1],
                [1, 0, 1, 0],
                [0, 1, 0],
                [1, 1],
            ],
        }
    )


@pytest.fixture
def fake_data(fake_df: pl.DataFrame) -> Data:
    return Data(
        fake_df.sort(["seq_len", "a"]).rename(
            {"a": "user", "b": "questions", "c": "answers"}
        ),
        n_questions=len(set().union(*(set(q) for q in fake_df["b"]))) + 1,
        batch_size=4,
    )


@pytest.fixture
def tiny_data(
    mocker: MockerFixture, tmp_path: Path
) -> tuple[Data, Data, Data, dict[int, int]]:
    mocker.patch(
        "kt.data.data._read_data",
        return_value=pl.read_csv(
            Path(__file__).parent / "data" / "synthetic_5" / "data.csv"
        ),
    )
    config = Config(
        user_field="student",
        timestamp_field="timestamp",
        question_field="question",
        answer_field="answer",
        p_valid=0.2,
        p_test=0.1,
    )
    df = read_data(config, tmp_path, tmp_path, echo=False)
    train_data, question2index = convert_data(
        df.filter(pl.col("dataset") == "train"),
        config,
        16,
        tmp_path,
        echo=False,
    )
    valid_data, _ = convert_data(
        df.filter(pl.col("dataset") == "valid"),
        config,
        16,
        tmp_path,
        question2index=question2index,
        echo=False,
    )
    test_data, _ = convert_data(
        df.filter(pl.col("dataset") == "test"),
        config,
        16,
        tmp_path,
        question2index=question2index,
        echo=False,
    )
    return train_data, valid_data, test_data, question2index
