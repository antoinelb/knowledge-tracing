from pathlib import Path

import pytest
from ruamel.yaml import YAML

from kt.experiment import Config


@pytest.fixture
def config() -> Config:
    return Config(
        name="test",
        model={"data": {"seed": 1}},
        eval={"data": {"seed": 1}, "metrics": ["accuracy"]},
    )


@pytest.fixture
def config_file(tmp_path: Path, config: Config) -> Path:
    path = tmp_path / "test.yml"
    yaml = YAML()
    yaml.dump(config.dict(), path)
    return path
