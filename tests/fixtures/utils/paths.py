from pathlib import Path

import pytest
from pytest_mock import MockerFixture


@pytest.fixture(autouse=True)
def root_dir(mocker: MockerFixture, tmp_path: Path) -> Path:
    path = tmp_path
    mocker.patch("kt.utils.paths.root_dir", path)
    return path


@pytest.fixture
def data_dir() -> Path:
    return Path(__file__).parent / ".." / "data"


@pytest.fixture
def model_dir(root_dir: Path) -> Path:
    return root_dir / "models"


@pytest.fixture
def cache_dir(root_dir: Path) -> Path:
    return root_dir / "cache"


@pytest.fixture
def results_dir(root_dir: Path) -> Path:
    return root_dir / "results"


@pytest.fixture
def mlflow_uri(root_dir: Path) -> str:
    return str(root_dir / "mlruns")
