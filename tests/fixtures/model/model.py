import jax
import jax.numpy as jnp
import pytest

from kt.model import ModelFct


@pytest.fixture
def model_fct() -> ModelFct:
    def fct(
        key: jax.random.KeyArray, inputs: dict[str, jnp.ndarray]
    ) -> dict[str, jnp.ndarray]:
        return {
            "predictions": (inputs["questions"] % 2 * 0.2)
            + 0.4
            + (-1 + (inputs["prev_answers"] * 2)) * 0.15
        }

    return fct
