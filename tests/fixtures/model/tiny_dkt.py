import pytest

from kt.model.models import ModelFcts
from kt.model.models.dkt import Config, create_model


@pytest.fixture
def tiny_dkt() -> ModelFcts:
    config = Config(type="dkt", n_hid=[16])
    return create_model(config)
