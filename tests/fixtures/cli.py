import pytest
import typer
from typer.testing import CliRunner

from kt.cli import init_cli


@pytest.fixture(scope="session")
def cli() -> typer.Typer:
    return init_cli()


@pytest.fixture(scope="session")
def runner() -> CliRunner:
    return CliRunner()
