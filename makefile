test:
	@pytest --cov --cov-report term-missing

static-analysis:
	@bandit -r src
	@black --check src tests
	@flake8 --statistics src tests
	@mypy src tests

mlflow:
	@mlflow ui --backend-store-uri .mlruns
