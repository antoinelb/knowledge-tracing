[![Pipeline](https://gitlab.com/antoinelb/knowledge-tracing/badges/main/pipeline.svg)](https://gitlab.com/antoinelb/knowledge-tracing/commits/main)
[![coverage report](https://gitlab.com/antoinelb/knowledge-tracing/badges/main/coverage.svg)](https://gitlab.com/antoinelb/knowledge-tracing/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black)
[![security: bandit](https://img.shields.io/badge/security-bandit-green.svg)](https://github.com/PyCQA/bandit)

# Knowledge tracing

Repository to do knowledge tracing experiments. 

[__Usage__](#usage)
| [__Setup__](#setup)
| [__Experiments__](#experiments)

## Usage

## Setup

### Install

1. Install [`poetry`](https://github.com/python-poetry/poetry)

2. `poetry config virtualenvs.in-project true`

3. `poetry install`

4. `source .venv/bin/activate`

### Synthetic data

To create synthetic datasets, the [`ktdg`](https://gitlab.com/antoinelb/ktdg) package is needed.
It can be installed with:

`poetry install --extras ktdg`


## Experiments

### Experiments on synthetic data

#### 1. Convergence of probability (todo)

##### Dataset

- single question repeated with no characteristics
- no learning
- random skill per student

##### Questions
- Does the model prediction converge to the right probability?
- Does the rolling standard deviation of predictions decrease monotonically?

#### 2. Independance of skills (todo)

##### Dataset
- 2 questions with different skill requirement
- no learning
- random independant skills per student

##### Questions
- What's the distribution of the impact on prediction of the other skill when answering a question from one of the skills?
- Is there a correlation between the model predictions between skills
- Does the order the skills are shown in have an influence (all of one and then all the other, interleaved, random, etc.)
- Does the magnitude of one skill influence the initial prediction of the other
